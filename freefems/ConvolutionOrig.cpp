// Example C++ function "Trivail matrix test .. ", dynamically loaded into "load.edp"
// ---------------------------------------------------------------------
// $Id$


#include "ff++.hpp"
#include <AFunction_ext.hpp>


static bool debug = false;

MatriceMorse<double> * Build2DMatrix(const Mesh & Th,double eps)
{

// construction of symmetric sparse matrix
//////////////////////////////////////////
    debug = (verbosity>999);
    int np=Th.nt; // number of triangle
    typedef  Mesh::Rd Rd;
    const int mp=Rd::d;
    Rd g=Rd::diag(1./(mp+1.));
    // pointer to build barycenters
//    double (*P)[mp] = new double[np][mp];
    KNM<double> P(mp,np);
    KN<double> mesures(np);
    double ep = eps*eps;
   
// compute barycenters
    for(int k=0; k<np; ++k){
        Rd A=Th[k](g);
        mesures(k) = Th[k].area;
        for(int i=0; i<mp; ++i)
            P(i,k)=A[i]; // all barycenters in P by cols
    }
  
    // vectors to get column indices and values
    std::vector<int> J;
    std::vector<double> AA;

    int nnz=0,kk,ll=0;
    double dist,valor;

    int * plg = new int[np+1];
    // loop for rows
    for (int i=0; i<np;++i)
    { 
        plg[i]=ll;
        kk = 0;
        for (int j=0; j<=i; ++j){
            dist = pow((P(0,i)-P(0,j)),2) + pow((P(1,i)-P(1,j)),2);
            if (dist < ep){  // barycenter(j) close to barycenter(i)          
                 valor = mesures(j)*max(0.,1-sqrt(dist)/eps) ;
                 if (valor > 1.e-14){
                  ++ll;
                  nnz++; 
                  J.push_back(j);
                  AA.push_back(valor); 
                  }
             }
        
        }
     }

    
    plg[np]=ll;
    
    int * scl = new int[nnz];
    double * sa = new double[nnz];
    // prepare data to assign to sparse matrix
    for (int i=0; i<nnz; i++){
        scl[i]=J[i];
        sa[i] = AA[i];
    }    

    // clean vectors
    J.clear();
    AA.clear();

///////////////////////////////////////////////////
// end of construction of symmetric sparse matrix

// symmetrization
//////////////////////////////////////////////////


    // count the new number of values
    int * counter = new int[np];
    for (unsigned int i=0; i<np; i++){
        counter[i]=plg[i+1] - plg[i];
        if( scl[plg[i+1]-1]==i) counter[i] -= 1; // remove diagonal element (already counted)
    }
    for (unsigned int i=0;i< nnz ;i++){
        counter[scl[i]] += 1;
    }

    // new pointer lg
    int * newlg = new int[np+1];
    newlg[0]=0;
  
    // compute newlg
    for (unsigned int i=1; i <np+1;++i)
        newlg[i] = newlg[i-1] +counter[i-1];
 
    // new pointers cl,a
    int newnnz = newlg[np];
    int * newcl = new int[newnnz];
    double * newa = new double[newnnz];

    
    // compute pointer where assign new values
    int * pointer = new int[np];
    for (unsigned int i=0; i< np; ++i)
        pointer[i] = newlg[i] + (plg[i+1]- plg[i]);
    
  
    // set the actual index recollocated
    int point;
    for (unsigned int i=0; i< np; i++){
          for (int j=0;j<plg[i+1]-plg[i];++j){
              newcl[newlg[i]+j] = scl[plg[i]+j];
              newa[newlg[i]+j] = sa[plg[i]+j];
          
              // set the new ones    
              if (scl[plg[i]+j] == i){
                  continue; // in case is diagonal element
                  }
              else{
                  point = pointer[scl[plg[i]+j]]++;
                  newcl[point] = i;                 
                  newa[point] = sa[plg[i]+j];
                  }
          }         
     }

    // free memory
    delete[] counter;
    delete[] pointer;
    delete[] sa;
    delete[] scl;
    delete[] plg;

// end of symmetrization
//////////////////////////////////////////////////

// normalization of values
//////////////////////////////////////////////////
    double suma;
    for (unsigned int i=0; i<np; ++i){
        suma = 0.;
        for (unsigned int j=newlg[i];j<newlg[i+1];j++){
            suma += newa[j];
        }
        for (unsigned int k=newlg[i];k<newlg[i+1];k++){
            newa[k] /= suma;
        }
    }

//    MatriceMorse<double> * ret = new MatriceMorse<double>(np,np,newnnz,false,newa,newlg,newcl);

    MatriceMorse<double> * mA = new MatriceMorse<double>(1,np);
    
    mA->j=0;
	mA->p=0;
	mA->aij=0;

	mA->set(np,np,0,newnnz,newlg,newcl,newa,0,1);

    // free memory
    delete[] newa;
    delete[] newlg;
    delete[] newcl;
  
   	return mA;
}




MatriceMorse<double> * Build3DMatrix(const pmesh3 & Th ,double eps)
{
// construction of symmetric sparse matrix
//////////////////////////////////////////
    debug = (verbosity>999);
    int np = Th->nt; // number of triangle
    typedef  Mesh3::Rd Rd;
    const int mp=Rd::d;
    Rd g=Rd::diag(1./(mp+1.));
    // pointer to build barycenters
    KNM<double> P(np,mp);
    KN<double> mesures(np);
    double ep = eps*eps;
   
// compute barycenters
 	for (int k=0;k< np;k++)
    { 
      const Tet & K( Th->elements[k] );
      Rd A = K(g);
      mesures(k) = K.mesure(); // volumes of tetrahedra
      for(int i=0; i<mp; ++i)
            P(k,i)=A[i]; // all barycenters in P by cols
    }
   
//     vectors to get column indices and values
    std::vector<int> J;
    std::vector<double> AA;

    int nnz=0,kk,ll=0;
    double dist,valor;

    int * plg = new int[np+1];
    // loop for rows
    for (int i=0; i<np;++i)
    { 
        plg[i]=ll;
        kk = 0;
        for (int j=0; j<=i; ++j){
            dist = pow((P(i,0)-P(j,0)),2) + pow((P(i,1)-P(j,1)),2) + 
            	   pow((P(i,2)-P(j,2)),2);
            if (dist < ep){  // barycenter(j) close to barycenter(i)          
                 valor = mesures(j)*max(0.,1-sqrt(dist)/eps) ;
                 if (valor > 1.e-14){
                  ++ll;
                  nnz++; 
                  J.push_back(j);
                  AA.push_back(valor); 
                  }
             }
        
        }
     }
    
    plg[np]=ll;
 

    int * scl = new int[nnz];
    double * sa = new double[nnz];
    // prepare data to assign to sparse matrix
    for (int i=0; i<nnz; i++){
        scl[i]=J[i];
        sa[i] = AA[i];
    }    

    // clean vectors
    J.clear();
    AA.clear();

///////////////////////////////////////////////////
// end of construction of symmetric sparse matrix

// symmetrization
//////////////////////////////////////////////////


    // count the new number of values
    int * counter = new int[np];
    for (unsigned int i=0; i<np; i++){
        counter[i]=plg[i+1] - plg[i];
        if( scl[plg[i+1]-1]==i) counter[i] -= 1; // remove diagonal element (already counted)
    }
    
    for (unsigned int i=0;i< nnz ;i++){
        counter[scl[i]] += 1;
    }
 
    // new pointer lg
    int * newlg = new int[np+1];
    newlg[0]=0;
  
    // compute newlg
    for (unsigned int i=1; i <np+1;++i)
        newlg[i] = newlg[i-1] +counter[i-1];
 

    // new pointers cl,a
    int newnnz = newlg[np];
    int * newcl = new int[newnnz];
    double * newa = new double[newnnz];

    
    // compute pointer where assign new values
    int * pointer = new int[np];
    for (unsigned int i=0; i< np; ++i)
        pointer[i] = newlg[i] + (plg[i+1]- plg[i]);
    
  
    // set the actual index recollocated
    int point;
    for (unsigned int i=0; i< np; i++){
          for (int j=0;j<plg[i+1]-plg[i];++j){
              newcl[newlg[i]+j] = scl[plg[i]+j];
              newa[newlg[i]+j] = sa[plg[i]+j];
          
              // set the new ones    
              if (scl[plg[i]+j] == i){
                  continue; // in case is diagonal element
                  }
              else{
                  point = pointer[scl[plg[i]+j]]++;
                  newcl[point] = i;                 
                  newa[point] = sa[plg[i]+j];
                  }
          }         
     }
    
    // free memory
    delete[] counter;
    delete[] pointer;
    delete[] sa;
    delete[] scl;
    delete[] plg;
    
// end of symmetrization
//////////////////////////////////////////////////

// normalization of values
//////////////////////////////////////////////////
    double suma;
    for (unsigned int i=0; i<np; ++i){
        suma = 0.;
        for (unsigned int j=newlg[i];j<newlg[i+1];j++){
            suma += newa[j];
        }
        for (unsigned int k=newlg[i];k<newlg[i+1];k++){
            newa[k] /= suma;
        }
    }

    
    MatriceMorse<double> * mA = new MatriceMorse<double>(1,np);
    
    mA->j=0;
	mA->p=0;
	mA->aij=0;

	mA->set(np,np,0,newnnz,newlg,newcl,newa,0,1);

    // free memory
    delete[] newa;
    delete[] newlg;
    delete[] newcl;
    
    return mA;
  
}




class Convolution3D:  public E_F0 { public:
    typedef Matrice_Creuse<double> * Result;
    Expression expTh,expeps;
    Convolution3D(const basicAC_F0 & args)
    {
        args.SetNameParam();
        expTh= to<pmesh3>(args[0]);
        expeps= CastTo<double>(args[1]);   
    }
    ~Convolution3D()
    {
    }
    
    static ArrayOfaType  typeargs() { return  ArrayOfaType(atype<pmesh3>(),atype<double>());}
    static  E_F0 * f(const basicAC_F0 & args){ return new Convolution3D(args);}
    AnyType operator()(Stack s) const ;    
};


AnyType Convolution3D::operator()(Stack stack) const
{
    Matrice_Creuse<double> * sparse_mat = new Matrice_Creuse<double>;
    // the code Add2StackOfPtr2Free is to remove memory after the usage of the pointeur ..
    pmesh3 pTh= GetAny<pmesh3>((*expTh)(stack));

    double eps=GetAny<double>((*expeps)(stack));
    Add2StackOfPtr2Free(stack,sparse_mat);
    MatriceMorse<double>  * amorse =  Build3DMatrix(pTh, eps);
    sparse_mat->Uh=UniqueffId();
    sparse_mat->Vh=UniqueffId();
    sparse_mat->A.master(amorse);
    sparse_mat->typemat= 0;
    // sparse_mat->typemat=(amorse->n == amorse->m) ? TypeSolveMat(TypeSolveMat::GMRES) : TypeSolveMat(TypeSolveMat::NONESQUARE); 
    
     return sparse_mat;
}


class Convolution2D:  public E_F0 { public:
    typedef Matrice_Creuse<double> * Result;
    Expression expTh,expeps;
    Convolution2D(const basicAC_F0 & args)
    {
        args.SetNameParam();
        expTh= to<pmesh>(args[0]);
        expeps= CastTo<double>(args[1]);   
    }
    
    ~Convolution2D()
    {
    }
    
    static ArrayOfaType  typeargs() { return  ArrayOfaType(atype<pmesh>(),atype<double>());}
    static  E_F0 * f(const basicAC_F0 & args){ return new Convolution2D(args);}
    AnyType operator()(Stack s) const ;    
};




AnyType Convolution2D::operator()(Stack stack) const
{
    Matrice_Creuse<double> * sparse_mat = new Matrice_Creuse<double>;
    
    // the code Add2StackOfPtr2Free is to remove memory after the usage of the pointeur ..
    pmesh pTh= GetAny<pmesh>((*expTh)(stack));
    double eps=GetAny<double>((*expeps)(stack));
    Add2StackOfPtr2Free(stack,sparse_mat);
    MatriceMorse<double>  * amorse =  Build2DMatrix(*pTh, eps);
    sparse_mat->Uh=UniqueffId();
    sparse_mat->Vh=UniqueffId();
    sparse_mat->A.master(amorse);
    sparse_mat->typemat= 0;
  
    // sparse_mat->typemat=(amorse->n == amorse->m) ? TypeSolveMat(TypeSolveMat::GMRES) : TypeSolveMat(TypeSolveMat::NONESQUARE); 
    
     return sparse_mat;
}




static void Load_Init()
{
    typedef Mesh3 *pmesh3;
    cout << " Convolution in process " << endl;
    Global.Add("Convolution3D","(", new OneOperatorCode<Convolution3D >( ));
    Global.Add("Convolution2D","(", new OneOperatorCode<Convolution2D >( ));
}
LOADFUNC(Load_Init)
