"""
Created on Sat Jul 15 14:04:05 2017

@author: earanda
"""

import pickle
import dictcode as d
import numpy as np
from extfun import is_number,checkempty
import os
from importlib import reload
    
def showmessage(s,t):
    print(s,t)

#def is_pos_def(x):
#    return np.all(np.linalg.eigvals(x) >= 0) and np.all(x == x.T) 

class Processor(object):
    def __init__(self,basepath='.',mainpath='.',fname='fichero.edp'):
        self.mes_error = ''
        self.load_error = 0
        self.fname = fname
        self.basepath = basepath
        self.mainpath = mainpath
        
    def dothejob(self,v):
        self.v = v
        if self.validating():
            if self.v['betaPROJECTION']:
                reload(d)    
            else:
                reload(d)
                import dictcodesinbeta as e
                reload(e)
                for x in e.codigo:
                    d.codigo[x] = e.codigo[x]
                    
            self.procesar()
#            showmessage("Info","File created")
        
    def procesar(self):
        """
        Processing values and building a file .edp
        """
        
        # processing elastic constants
        myoung = 'real E1 = ' + self.v['young'] + ';'
        vyoung = 'real E0 = ' + self.v['young_void'] + ';'
        cpoisson = 'real nu = ' + self.v['poisson'] + ';'
        econstants = myoung + '\n' + vyoung + '\n' + cpoisson + '\n'
        nombrevtk = os.path.join(self.basepath,self.v['namefile'])
        resultadop = os.path.join(self.basepath,self.v['resultadopure'])
        resultadof = os.path.join(self.basepath,self.v['resultadofilter'])
        archivothres = os.path.join(self.basepath,self.v['namefile'] + '-threshold.vtk');
        d.codigo['savenumeric'] = d.codigo['savenumeric'].format(
                resultadopure=resultadop,resultadofilter=resultadof)

        
        # Lame coefficient
        if self.v['tensionplana']:
            poisson = 'real lambda1 = nu/(1.-nu^2);'
        else:
            poisson = 'real lambda1 = nu/((1.+nu)*(1.-2*nu)); '
        poissonval = float(self.v['poisson'])
        
        # choose dimension
        if self.v['dimension'] == 2:
            wordintb = 'int1d'
            wordintt = 'int2d'
            wordmesh = 'mesh'
            readmesh = 'readmesh'
            sufdim = '_2d'
            gravity_direction = 'v2'
            density_direction = 'u2'
            plot = d.plot
            plotd = d.plotd2.format(name=nombrevtk)
            dim = '2D'
            gmshload = "gmshload"
            spadj = ', [q1,q2]'
            pp = max(2./(1-poissonval),4./(1+poissonval))
        else:
            wordintb = 'int2d'
            wordintt = 'int3d'
            wordmesh = 'mesh3'
            readmesh = 'readmesh3'
            sufdim = '_3d'
            gravity_direction = 'v3'
            density_direction = 'u3'
            plot = ''
            plotd = d.plotd3.format(name=nombrevtk)
            dim = '3D'
            gmshload = "gmshload3"
            spadj = ', [q1,q2,q3]'
            pp = max(15.*(1-poissonval)/(7-5*poissonval), \
                     3./2*(1-poissonval)/(1-2*poissonval))
            
        # check validity of SIMP parameter p
        if int(self.v['simp']) < pp:
            showmessage("Warning",\
            "SIMP penalization does not satisfy correct bounds. Minimum value is {0}".format(pp))
            
        # total volume
        medida = 'real medida = ' + wordintt + '(Th)(1.); // total domain volume'
        
        ######################################################################
        # processing functions and integrals
        self.functions = ''
        integrals = ''
        objective = ''
        bilinearpart = ''
        
        # processing Robin conditions
        # processing spring
        for j,x in enumerate(self.v['robin']):
            intarray = np.array(['','',''],dtype=object)
            for i in range(2):
                intarray[i] = self.buildmatrixloads(x[0],j,i,'rb')
            if self.v['dimension'] == 3:
                intarray[2] = self.buildmatrixloads(x[0],j,2,'rb')

            bintarray = (intarray != '')
            if bintarray.sum():
                integrand = '( ' + ' + '.join(intarray[bintarray]) + ' ) '
            
                etiquetas = ','.join((x[2]).split())
                if etiquetas:
                    etiquetas = ',' + etiquetas
                bilinearpart += '+ ' + wordintb + '(Th' + etiquetas + ')' + integrand
        if bilinearpart:
            bilinearpart += '\n'
        
        # processing force of Robin
        for j,x in enumerate(self.v['robin']):
            intarray = np.array(['','',''],dtype=object)
            for i,prex in enumerate(('f','g')):
                intarray[i] = self.buildloads(x[1],j,i,prex,'rb')
            if self.v['dimension'] == 3:
                intarray[2] = self.buildloads(x[1],j,2,'h','rb')

            bintarray = (intarray != '')
            if bintarray.sum():
                integrand = '( ' + ' + '.join(intarray[bintarray]) + ' ) '
            
                etiquetas = ','.join((x[2]).split())
                if etiquetas:
                    etiquetas = ',' + etiquetas
                integrals += '- ' + wordintb + '(Th' + etiquetas + ')' + integrand
                objective += '+ ' + wordintb + '(Th' + etiquetas + ')' + integrand
        integrals += '\n'
        
        # boundary and inner loads
        for y,fun,eti in zip(['bd_loads','in_loads'],['bd','in'],[wordintb,wordintt]):
            for j,x in enumerate(self.v[y]):
                intarray = np.array(['','',''],dtype=object)
                for i,prex in enumerate(('f','g')):
                    intarray[i] = self.buildloads(x,j,i,prex,fun)
                if self.v['dimension'] == 3:
                    intarray[2] = self.buildloads(x,j,2,'h',fun)

                bintarray = (intarray != '')
                if bintarray.sum():
                    integrand = '( ' + ' + '.join(intarray[bintarray]) + ' ) '
                
                    etiquetas = ','.join((x[3]).split())
                    if etiquetas:
                        etiquetas = ',' + etiquetas
                    integrals += '- ' + eti + '(Th' + etiquetas + ')' + integrand
                    objective += '+ ' + eti + '(Th' + etiquetas + ')' + integrand

        # self-weight
        if self.v['density_tick']:
            if is_number(self.v['density']):
                    self.functions += 'real density = -9.81*(' + self.v['density'] + ');\n'
            else:
                    self.functions += 'func density = -9.81*(' + self.v['density'] + ');\n'
                    
            objective += '+ ' + wordintt + '(Th)(density*rhobar*' + gravity_direction + ')'
            integrals += '- ' + wordintt + '(Th)(density*rhobar*' + gravity_direction + ')'
            d.main_names['variational_derivatives'] = 'variational_derivatives_weighted'
            uu = density_direction
        else:
            uu = ''             
# --------------------------------------------------------------------------------------------

            
        # processing mechanism problems (for adjoint problem)
        if self.v['problem'] == 'Mechanism':
            objective = ''
            integraladj = bilinearpart
            for j,x in enumerate(self.v['obj_mechanism']):
                intarray = np.array(['','',''],dtype=object)
                for i,prex in enumerate(('f','g')):
                    intarray[i] = self.buildloads(x,j,i,prex,'objm')
                if self.v['dimension'] == 3:
                    intarray[2] = self.buildloads(x,j,2,'h','objm')  
                bintarray = (intarray != '')
                if bintarray.sum():
                    integrand = '( ' + ' + '.join(intarray[bintarray]) + ' ) '
                    etiquetas = ','.join((x[3]).split())
                    if etiquetas:
                        etiquetas = ',' + etiquetas
                    objective += ' + ' + wordintb + '(Th' + etiquetas + ')' + integrand
                    integraladj += ' - ' + wordintb + '(Th' + etiquetas + ')' + integrand
        else:
            spadj = ''

# ---------------------------------------------------------------------------------------------            
                
        d.codigo['func_definition'] = self.functions
        d.codigo['acting_forces'] = bilinearpart + integrals
        obj = ((objective.replace('v1','u1')).replace('v2','u2')).replace('v3','u3')
        
        # processing clamped conditions
        dirichlet = ''
        for x,y in zip(('X','Y','Z','XY','XZ','YZ','XYZ'),('u1=0','u2=0','u3=0','u1=0,u2=0',\
                       'u1=0,u3=0','u2=0,u3=0','u1=0,u2=0,u3=0')):        
            if self.v['clamped_tick'][x]:
                dirichlet += ' + on(' + ','.join(self.v['clamped'][x].split()) + ',' + y + ')'

                
        dirichlet += ';'
        
        d.codigo['clamped_conditions'] = dirichlet
        
        # for mechanism problem
        if self.v['problem'] == 'Mechanism':
            d.main_names['adjoint'] = 'adjoint' + sufdim
            clamped_adj = ((dirichlet.replace('u1','q1')).replace('u2','q2')).replace('u3','q3')
            integraladjoint = ((integraladj.replace('u1','q1')).replace('u2','q2')).replace('u3','q3')
            d.codigo[d.main_names['adjoint']] = d.codigo[d.main_names['adjoint']].format(force=integraladjoint,dirichlet=clamped_adj)
            d.main_names['variational_derivatives'] = 'variational_derivatives_adj'
            deradj = 'Adjoint;'
        else:
            d.codigo[d.main_names['adjoint']] = ''
            deradj = ''

# -----------------------------------------------------------------------------------------------
        
        # select objective functional
    
        
        # volume problem
        if self.v['problem'] == 'Volume':
            func_comp = 'real compobj = ' + obj + ';'
            comfrac = 'real Compfrac = compobj/' + self.v['compliance'] + ';'
            d.codigo['compliance_constant'] = d.codigo['compliance_constant'].format(\
                    compliance=func_comp,comfrac=comfrac)            
            comp_const = 'real complobj = ' + obj + ';'            
            d.main_names['constant_compliance'] = 'compliance_constant'
            func_objective = 'real obj = 1./medida*' + wordintt + '(Th)(rhobar) ;'
            d.main_names['objective_gradient'] = 'objective_volume_gradient_' 
            d.main_names['volume_gradient'] = 'compliance_gradient_'   
            volfrac = '1.00'
        else:                   
            func_objective = 'real obj = ' + obj + ';'
            d.codigo['compliance_normalization'] = d.codigo['compliance_normalization'].format(\
                    compliance=func_objective)     
            volfrac = self.v['volfrac']

# ------------------------------------------------------------------------------------------------        
        # processing standars
    
        meshfile = os.path.abspath(os.path.normpath( (os.path.join(self.mainpath,self.v['path'],self.v['meshfile']))))
        rutainit = os.path.normpath(os.path.join(self.mainpath,self.v['init_file']))
    
        if self.v['gmsh_mesh']:
            malla = wordmesh + ' Th = ' + gmshload + '("' + meshfile + '");'
            d.codigo['previous'] = d.codigo['previous'] + d.code_gmsh + '\n'
        else:
            malla = wordmesh + ' Th = ' + readmesh + '("' + meshfile + '");'


        if self.v['betaPROJECTION']:
            betaup = self.v['betaup']
            betaloop = self.v['betaloop']
        else:
            betaup = betaloop = ''


        d.codigo['mesh'] = d.codigo['mesh'].format(mesh=malla)
        d.codigo['parameters'] = d.codigo['parameters'].format(young=econstants,\
                lambda1=poisson, medida=medida,penal=self.v['simp'], \
                betaupdate=betaup,loops=betaloop, \
                maxiter=self.v['maxiter'],volume=volfrac, initcte=self.v['init_cte'], \
                tolerance=self.v['tol'],filtro=self.v['filter'])
        d.main_names['elasticity_system'] = 'elasticity_system' + sufdim
        
        d.codigo[d.main_names['variational_derivatives']] = \
            d.codigo[d.main_names['variational_derivatives']].format(integral=wordintt,uu=uu)
        
        
        
        # processing filter
        if self.v['methodFILTER'] == "Conic":
            suffilter = 'conic'
            d.codigo['previous'] = d.codigo['previous'] + d.code_convolution + '\n'
            d.codigo['filter_conic'] = d.codigo['filter_conic'].format(dim=dim)            
        elif self.v['methodFILTER'] == "Hemholz":
            suffilter = 'hemholz'
            if self.v['methodOPT'] == "MMA":
                bisfilter = ''
            else:
                bisfilter = 'varf filterk(unused,h2) = int2d(Th)(Rk*h2);'                
            d.codigo['filter_hemholz'] = d.codigo['filter_hemholz'].format(integral=wordintt,\
                    bisection_filter=bisfilter)
        else:
            showmessage("Warning","No filter has been chosen")
            reload(d)
            return
        
        if self.v['betaPROJECTION'] and self.v['methodOPT'] != "OC":
            betaup = "beta /= betaupdate;"
        else:
            betaup = ""
         
        if self.v['methodOPT'] == 'IPOPT':
            sufixmethod = 'ipopt'
            d.main_names['objective'] = 'objective_ipopt_' + suffilter        
            d.main_names['prebeta'] = 'prebeta_' + sufixmethod 
            if self.v['problem'] != 'Volume':
                d.main_names['volume_constraint'] = 'volume_constraint_ipopt_' + suffilter
            else:
                d.main_names['objective'] = 'objective_volume_ipopt_' + suffilter
                d.main_names['volume_constraint'] = 'compliance_constraint_ipopt_' + suffilter
                
        elif self.v['methodOPT'] == 'MMA':
            sufixmethod = 'mma'
            d.main_names['objective'] = 'objective_mma'
            d.main_names['prebeta'] = 'prebeta_mma_' + suffilter
            if self.v['problem'] != 'Volume':
                d.main_names['volume_constraint'] = 'volume_constraint_mma'
            else:
                d.main_names['objective'] = 'objective_volume_mma'
                d.main_names['volume_constraint'] = 'compliance_constraint_mma'
        else:
            sufixmethod = ''
            d.main_names['objective'] = 'objective_mma'
            d.main_names['volume_constraint'] = 'volume_constraint_mma'
                

               
        if self.v['problem'] != "Volume":
            d.codigo[d.main_names['volume_constraint']] = d.codigo[d.main_names['volume_constraint']].format(integral=wordintt)    
        else:
            d.codigo[d.main_names['volume_constraint']] = \
              d.codigo[d.main_names['volume_constraint']].format(funcional=comp_const)     

        
        d.main_names['spaces'] = 'spaces_' + suffilter + sufdim
        d.main_names['filtro'] = 'filter_' + suffilter
        d.main_names['sin_beta'] = 'sin_beta_' + suffilter
        d.main_names['stress'] = 'vonmisses' + sufdim
        d.main_names['final_update'] = 'final_update_' + suffilter
        d.main_names['iterations'] = 'optimality_' + suffilter
        d.codigo[d.main_names['objective']] = \
              d.codigo[d.main_names['objective']].format(funcional=func_objective,plot=plot)   
              
        if self.v['methodOPT'] != 'OC':
            d.main_names['objective_gradient'] = d.main_names['objective_gradient'] + sufixmethod + '_' + suffilter
            d.main_names['volume_gradient'] = d.main_names['volume_gradient'] + sufixmethod + '_' + suffilter
            d.main_names['beta_loops'] = 'beta_loops_' + sufixmethod 
#            d.codigo[d.main_names['prebeta']] = d.codigo[d.main_names['prebeta']].format(adj=deradj)
            d.codigo[d.main_names['objective_gradient']] = \
                d.codigo[d.main_names['objective_gradient']].format(adj=deradj)
       
        d.codigo[d.main_names['spaces']] = d.codigo[d.main_names['spaces']].format(adj=spadj)
        d.codigo[d.main_names['final_update']] = \
          d.codigo[d.main_names['final_update']].format(plotd=plotd,beta=betaup)

        # processing passive zone
        if self.v['passive_tick']:
            if checkempty(self.v['passive']):
                pasivo = 'varf zonapasiva(unused,v) = ' + wordintt + '(Th,' + \
                        ','.join(self.v['passive'].split()) + ')(v);'
                if self.v['init_random']:
                    d.main_names['initialization'] = 'passive_random_initialization'
                    d.codigo['passive_random_initialization'] = \
                      d.codigo['passive_random_initialization'].format(pasivo=pasivo,integral=wordintt)
                elif not self.v['init_file']:
                    d.main_names['initialization'] = 'passive_initialization'
                    d.codigo['passive_initialization'] = \
                      d.codigo['passive_initialization'].format(pasivo=pasivo,integral=wordintt)
                else:
                    d.main_names['initialization'] = 'passive_force_initialization'
                    d.codigo['passive_force_initialization'] = d.codigo['passive_force_initialization'].format(
                            pasivo=pasivo,integral=wordintt,name=rutainit)
            else:
                showmessage("Error","Passive zone is set but there is no label")
                return
        else:
            if self.v['init_file']:
                d.main_names['readnumeric'] = 'force_initialization'
                d.codigo['force_initialization'] = \
                  d.codigo['force_initialization'].format(name=rutainit)
            elif self.v['init_random']:
                d.main_names['readnumeric'] = 'random_initialization'
            
            
        # processing saving and loading numeric result or random initialization
        
        # load ffrandom
        if self.v['init_random']:
            d.codigo['previous'] = d.codigo['previous'] + d.code_random + '\n'
            
        d.codigo['threshold'] = d.codigo['threshold'].format(integral=wordintt,
                dibujo=archivothres)

        # writing file
        with open(self.fname,'w') as fichero:
        
            if self.v['methodOPT'] == "MMA":
                d.codigo['previous'] = d.codigo['previous'] +  d.code_mma + '\n'
                for name in d.mma:
                    fichero.write(d.codigo[d.main_names[name]])
            elif self.v['methodOPT'] == "IPOPT":
                d.main_names['mma_variables'] = 'ipopt_variables'
                d.codigo['previous'] = d.codigo['previous'] +  d.code_ipopt + '\n'
                for name in d.mma:
                    fichero.write(d.codigo[d.main_names[name]])
            else:
                d.codigo['parameters'] = d.codigo['parameters'] + \
                d.codigo['parameters_optimality'].format(zeta=str(self.v['zeta']),\
                        eta=str(self.v['eta']))
                volrhobar = 'real vol = ' + wordintt + '(Th)(Rhobar);'
                d.main_names['initialization'] = 'init'
                d.codigo[d.main_names['iterations']] = \
                d.codigo[d.main_names['iterations']].format(volrhobar=volrhobar)
                for name in d.optimality:
                    fichero.write(d.codigo[d.main_names[name]])
                     
        
        
        


    def buildloads(self,x,j,i,prex,fun,extra=False):
        if checkempty(x[i]):                
            if is_number(x[i]):
                self.functions += 'real ' + prex + str(j) + fun + ' = ' + x[i] + ';\n'
            else:
                self.functions += 'func ' + prex + str(j) + fun + ' = ' + x[i] + ';\n'    
            iarray = prex  + str(j) + fun + '*v' + str(i+1) 
        else:
            iarray = ''
        return iarray
    
       
    def buildmatrixloads(self,x,j,i,fun):
        iarray = np.array(['','',''],dtype=object)
        rarray = ''
        for k in range(3):
            if checkempty(x[i][k]):                
                if is_number(x[i][k]):
                    self.functions += 'real m' + str(j) + 't' + str(i) + str(k) + fun + ' = ' + x[i][k] + ';\n'
                else:
                    self.functions += 'func m' + str(j) + 't' + str(i) + str(k) + fun + ' = ' + x[i][k] + ';\n'
                iarray[k] = 'm'  + str(j) + 't' + str(i) + str(k) + fun + '*u' + str(k+1) 
            else:
                iarray[k] = ''
            
        bintarray = (iarray != '')
        if bintarray.sum() != 0:
            rarray += '( ' + ' + '.join(iarray[bintarray]) + ' )*v'+str(i+1)
        return rarray


        
    def validating(self):
        """
        Validate data
        """            
        
        # any load (inner, boundary or robin)?
        if self.checkloads():
            showmessage("Error","There are no loads")
            return False
            
        
        # check that there is a label when is necessary
        val = ['passive','density']
        messages = ['Passive Zone','Self-Weight']
        for x,m in zip(val,messages):
            y = self.v[x+'_tick']
            self.is_nolabel(y,self.v[x],m)

        for x in ['X','Y','Z','XY','XZ','YZ','XYZ']:
            self.is_nolabel(self.v['clamped_tick'][x],self.v['clamped'][x],\
                            'Clamped condition at '+x)
            
        # check if robin condition are well posed
        for x in self.v['robin']:
            self.is_robin(x)
        
        if self.v['problem'] == 'Mechanism':
            self.is_noload('obj_mechanism','Objective')
            
        if not self.v['meshfile']:
            self.mes_error += 'There is no mesh'
            
        if not self.mes_error:
            return True
        else:
            showmessage("Errors",self.mes_error)
            return False
        
    def is_nolabel(self,val,label,message):
        if val and not label: 
            self.mes_error += message + ' is set but label is empty\n'
                
    def is_noload(self,load,message):
        if not isinstance(self.v[load],list):
            self.mes_error += message + 'is not a list\n'
        err = 0
        for x in self.v[load]:
            if self.v['dimension'] == 2:
                if not x[0] and not x[1]:        
                    err = 1
                    self.mes_error += "No force in " + message + "\n"
            else:
                if not x[0] and not x[1] and not x[2]:
                    err = 1
                    self.mes_error += "No force in " + message + "\n"
            if not err and not x[3] and (load == 'bd_loads' or load == 'obj_mechanism'):
                self.mes_error += "Label for " + message + " is not set\n"
        
    def is_robin(self,x):
        if not isinstance(x,list):
            self.mes_error += 'Robin condition is not a list\n'
            return
        # check if matrix is definite positive
#        n = self.v['dimension']
#        mat = np.zeros((n,n))
#        for i in range(n):
#            for j in range(n):
#                if checkempty(x[0][i][j]):
#                    mat[i,j] = float(x[0][i][j])
#        
#        if not is_pos_def(mat):
#            self.mes_error += "Rigidity matrix is not symmetric positive definite\n"
        
        self.is_nolabel(x[0],x[2],'Robin')       
    
    def checkloads(self):
        a = ['','','','']
        if a in self.v['in_loads'] and a in self.v['bd_loads'] and \
            len(self.v['robin'])==0 and not self.v['density_tick']:
            return True
        else:
            return False
        
if __name__ == "__main__":
    filename = "nuevo.topt"

    with open(filename,"rb") as input_file:
        v = pickle.load(input_file)
    a = Processor()
    a.dothejob(v)
    
    
