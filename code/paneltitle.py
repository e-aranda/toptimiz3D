# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:51:46 2017

@author: earanda
"""
import wx
import os
import popupwindow


# ----------------------------------------------------------------------------
class MyPanelTitle(wx.Panel):
    """
    Panel for main title in panel
    """
    def __init__(self,parent,title):
        wx.Panel.__init__(self,parent)
        self.padre = parent
        self.title = title
        
        t_sizer = wx.BoxSizer(wx.VERTICAL)
        t1 = wx.StaticText(self,-1,title,style = wx.EXPAND|wx.ALL|wx.ALIGN_CENTER)
        font = wx.Font(12,wx.FONTFAMILY_MODERN,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_BOLD)
        t1.SetFont(font)
#        t_sizer.Add(t1,1,wx.ALL|wx.ALIGN_TOP|wx.ALIGN_CENTER, 5)
        t_sizer.Add(t1,1,wx.EXPAND|wx.ALL|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 5)
        
        
        # Add Help button
        addbot = wx.Panel(self)        
        b_sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        CURRENT_DIR = os.path.dirname(__file__)
        pngfile = os.path.join(CURRENT_DIR, os.path.join('icons','help.png'))   
        png = wx.Image(pngfile, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bot = wx.BitmapButton(addbot, -1, png)

#        bot.SetBackgroundColour(wx.Colour(0, 0, 255))        
#        bot.Bind(wx.EVT_BUTTON, self.onShowPopup)

        
        # adding to sizers
        b_sizer.Add(bot,1,wx.RIGHT|wx.TOP,2)
        
        t_sizer.Add(addbot,0,wx.ALL|wx.ALIGN_RIGHT,5)
        # set sizer        
        addbot.SetSizer(b_sizer)
        
        self.SetSizer(t_sizer)
        
        

    def onShowPopup(self, event):
        """
        Click help button: create a frame and show help
        """
        self.help_frame = popupwindow.WinFrame(self.padre,self.title)
        self.help_frame.Show()
        
