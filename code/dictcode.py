# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:12:33 2017

@author: earanda

FreeFem codes for topology optimization problem
"""

codigo = {}

# carga de paquetes
codigo['previous'] = """
verbosity = 0;
include "getARGV.idp";
include "colormap.idp";
load "iovtk";
load "MUMPS_seq";
load "medit";
"""

# carga de paquetes adicionales
code_convolution = 'load "ConvolutionOrig";'
code_mma = 'load "ff-NLopt";'
code_ipopt = 'load "ff-Ipopt";'
code_gmsh = 'load "gmsh";'
code_random = 'load "gsl";'


# lectura de malla (desde fichero)
codigo['mesh'] = """

{mesh}
cout << "Mesh info - number of triangles: " << Th.nt << endl;
"""


# parámetros del problema
codigo['parameters'] = """

//*************************************
// PARAMETERS
//*************************************

// elastic parameters
{young}
real E10 = E1-E0;
// Lamé parameters: 2*mu = mu1*E, lambda = lambda1*E
real mu1 = 1./(1+nu); 
{lambda1}

// SIMP parameter
int p = {penal};

// filter parameter
real rfilter= {filtro};

// projection parameters
real beta=1., eta=0.5;

// projection values
//real tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
//real tb = tanh(beta*eta)*tbi;
real tb = 0.5;
real tbi = 1./(2.*tanh(0.5*beta));

// maximum of iterations of one call of MMA
int maxiter = {maxiter};

// beta loop parameters
int loops = {loops};
int betaupdate = {betaupdate};

// tolerance (for MMA)
real starttol = {tolerance};

// constant for initialization
real Initcte = {initcte};

// maximum volume allowed
real Volfrac = {volume};
{medida}
real Vol = Volfrac*medida;


"""

codigo['parameters_optimality'] = """
// optimality conditions parameters
real zeta = {zeta};
real neta = {eta};
"""


# definiciones de espacios

codigo['spaces_conic_2d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1]); // displacements
fespace Wh(Th,P0); // density

Vh [u1,u2], [v1,v2] {adj};
Wh rho,rhobar,rhosharp,psip,rhohat,Rk;
"""

codigo['spaces_hemholz_2d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1]); // displacements
fespace Wh(Th,P0); // density
fespace Zh(Th,P1); // filter

Vh [u1,u2], [v1,v2] {adj};
Wh rho,rhobar,rhosharp,psip,Rk;
Zh rhohat;
"""


codigo['spaces_conic_3d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1,P1]); // displacements
fespace Wh(Th,P0); // density

Vh [u1,u2,u3], [v1,v2,v3] {adj};
Wh rho,rhobar,rhosharp,psip,rhohat,Rk;
"""

codigo['spaces_hemholz_3d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1,P1]); // displacements
fespace Wh(Th,P0); // density
fespace Zh(Th,P1); // filter

Vh [u1,u2,u3], [v1,v2,v3] {adj};
Wh rho,rhobar,rhosharp,psip,Rk;
Zh rhohat;
"""




# sistema de elasticidad

codigo['elasticity_system_2d'] = """

//**********************************************
// VARIATIONAL FORMULATIONS
//**********************************************

// macros for simplify notation
real sqrt2=sqrt(2.);
macro epsilon(u)  [dx(u#1),dy(u#2),(dy(u#1)+dx(u#2))/sqrt2] // EOM
macro div(u) ( dx(u#1)+dy(u#2) ) // EOM
macro E(rho) ( E0 + rho^p*E10 )// EOM
macro gradient(hat)  [dx(hat),dy(hat)] // EOM

// elasticity system
problem Lame([u1,u2],[v1,v2],solver=sparsesolver)=
  int2d(Th)( E(rhobar)*(lambda1*div(u)*div(v)	
	    + mu1*(epsilon(u)'*epsilon(v)) ) )
"""

codigo['elasticity_system_3d'] = """

//**********************************************
// VARIATIONAL FORMULATIONS
//**********************************************

// macros for simplify notation
real sqrt2=sqrt(2.);
macro epsilon(u)  [dx(u#1), dy(u#2), dz(u#3), (dz(u#2)+dy(u#3))/sqrt2, (dz(u#1)+dx(u#3))/sqrt2,
                   (dy(u#1)+dx(u#2))/sqrt2] // EOM
macro div(u) ( dx(u#1)+dy(u#2)+dz(u#3) ) // EOM
macro E(rho) ( E0 + rho^p*E10 )// EOM
macro gradient(hat)  [dx(hat),dy(hat),dz(hat)] // EOM

// elasticity system
problem Lame([u1,u2,u3],[v1,v2,v3],solver=sparsesolver)=
  int3d(Th)( E(rhobar)*(lambda1*div(u)*div(v)	
	    + mu1*(epsilon(u)'*epsilon(v)) ) )
"""

codigo['adjoint_2d'] = """

problem Adjoint([q1,q2],[v1,v2],solver=sparsesolver)=
  int2d(Th)( E(rhobar)*(lambda1*div(q)*div(v)	
	    + mu1*(epsilon(q)'*epsilon(v)) ) )
  {force}
  {dirichlet}
  
"""

codigo['adjoint_3d'] = """

problem Adjoint([q1,q2,q3],[v1,v2,v3],solver=sparsesolver)=
  int3d(Th)( E(rhobar)*(lambda1*div(q)*div(v)	
	    + mu1*(epsilon(q)'*epsilon(v)) ) )
  {force}
  {dirichlet}
  
"""

# derivadas variacionales del compliance y el volumen

codigo['variational_derivatives']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*psip*( mu1*epsilon(u)'*epsilon(u)
+ lambda1*div(u)^2 )*v ) {uu}; 

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(psip*v); 
"""

codigo['variational_derivatives_adj']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*psip*( mu1*epsilon(u)'*epsilon(q)
+ lambda1*div(u)*div(q) )*v ) {uu}; 

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(psip*v); 
"""


codigo['variational_derivatives_weighted']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*psip*( mu1*epsilon(u)'*epsilon(u)
                     + lambda1*div(u)^2 )*v ) + {integral}(Th)(2*density*{uu}*psip*v);

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(psip*v); 
"""



# filtros

codigo['filter_conic'] = """

//*********************************************
// CONIC FILTER
//*********************************************

real time1 = clock();
cout << "Initiating filtering matrix:" << endl;
matrix A = Convolution{dim}(Th,rfilter);
real time2 = clock()-time1;
cout << "Time computing filtering matrix: " << time2 << endl;
"""


codigo['filter_hemholz'] = """

//*******************************
// HELMHOLZ TYPE FILTER
//*******************************

// filter problem
real rr2 = rfilter^2;
varf matrixfilter(h1,h2) = {integral}(Th)(rr2 * (gradient(h1)'*gradient(h2))) + {integral}(Th)(h1*h2);
varf dfilter(h1,h2) = {integral}(Th)(h1*h2);
varf filter(unused,h2) = {integral}(Th)(rho*h2);

{bisection_filter}

real time1 = clock();
cout << "Initiating filtering and interpolation matrices:" << endl;
// matrices of filter problem
matrix A = matrixfilter(Zh,Zh,solver=sparsesolver);
matrix B = dfilter(Wh,Zh);
// interpolation matrix between P1 and P0
matrix PI = interpolate(Wh,Zh);
real time2 = clock()-time1;
cout << "Time computing filtering and interpolation matrices: " << time2 << endl;
"""





codigo['sin_beta_conic'] = """

"""

codigo['sin_beta_hemholz'] = """

"""


# variables para MMA

codigo['mma_variables'] = """

//**************************************
// MMA VARIABLES
//**************************************

int nv = Th.nt; // number of variables
int nc = 1; //  number of constraints
// arrays for MMA
real[int] Xini(nv),Xmax(nv),Xmin(nv),rF(nc),retJ(nv);
real[int,int] retF(nc,nv);
// external counter
int jj=0;
"""

# variables para MMA

codigo['ipopt_variables'] = """

//**************************************
// IPOPT VARIABLES
//**************************************

int nv = Th.nt; // number of variables
int nc = 1; //  number of constraints
// arrays for MMA
real[int] Xini(nv),Xmax(nv),Xmin(nv),rF(nc),retJ(nv),Shand(nc);
matrix retF;
// external counter
int jj=0;
"""


#### 
#  if (mobj < realobj){{
#  cout << "Iteration: " << ++realiter  << " Objective: " << mobj << " " ;
#  }}
#  realobj = mobj;

# objective function
####

codigo['objective_mma'] = """

// macro function with objective
func real J(real[int] & xx){{

{funcional}
real mobj = Mobj*obj; // normalized cost

  cout << "Func. evaluation: " << ++jj << "  Objective: " << mobj << " (" << obj << ") " ; 

{plot}

  return mobj;
}}
"""

codigo['objective_ipopt_conic'] = """
// macro function with objective
func real J(real[int] & xx){{

    rho[] = xx;
    rhohat[] = A*rho[]; // filter 
    rhobar = tb + tbi*tanh(beta*(rhohat-eta));
    Lame;       

{funcional}
real mobj = Mobj*obj; // normalized cost

  cout << "Func. evaluation: " << ++jj << "  Objective: " << mobj << " (" << obj << ") " ; 

{plot}

  return mobj;
}}
"""

codigo['objective_ipopt_hemholz'] = """
// macro function with objective
func real J(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;

{funcional}
real mobj = Mobj*obj; // normalized cost

  cout << "Func. evaluation: " << ++jj << "  Objective: " << mobj << " (" << obj << ") " ; 

{plot}

  return mobj;
}}
"""




codigo['plot'] = """
if(jiter % jplot == 0 && mobj < realobj){{
jiter++;
mobj = realobj;
{plot}
}}
"""


# volume constraint function

codigo['volume_constraint_mma'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

        
  real volumen = 1./Vol*{integral}(Th)(rhobar)-1.; //normalized volume constraint
  rF[0] = volumen;
  cout << " Restr: " << volumen << endl;
  
  return rF;
}}
"""

codigo['volume_constraint_ipopt_conic'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

  rho[] = xx;    
  rhohat[] = A*rho[]; // filter 
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));      
  
  real volumen = 1./Vol*{integral}(Th)(rhobar)-1.; //normalized volume constraint
  rF[0] = volumen;
  cout << " Restr: " << volumen << endl;
  
  return rF;
}}
"""

codigo['volume_constraint_ipopt_hemholz'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));

  real volumen = 1./Vol*{integral}(Th)(rhobar)-1.; //normalized volume constraint
  rF[0] = volumen;
  cout << " Restr: " << volumen << endl;
  
  return rF;
}}
"""





# objective compliance gradient

codigo['objective_gradient_mma_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));
  Lame;
  
  {adj}
 
        
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
  real[int] c=der(0,Wh); // objective's gradient
  real[int] retaJ = A*c;
  retJ = Mobj*retaJ;
  
  return retJ;

}}
"""

codigo['objective_gradient_ipopt_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));
  Lame;
  
  {adj}
        
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
  real[int] c=der(0,Wh); // objective's gradient
  real[int] retaJ = A*c;
  retJ = Mobj*retaJ;
  
  return retJ;

}}
"""




codigo['objective_gradient_mma_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;

  {adj}
  
  
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
  real[int] c=der(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] retaJ = B'*c2;
  retJ = Mobj*retaJ;

  return retJ;
}}
"""





codigo['objective_gradient_ipopt_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation  
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;

  {adj}

  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
  real[int] c=der(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] retaJ = B'*c2;
  retJ = Mobj*retaJ;
  
  return retJ;
}}
"""

# volume constraint gradient

codigo['volume_gradient_mma_conic'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){

  real[int] dc = dconsres(0,Wh);
  real[int] d1 = A*dc;

  retF(0,:) = 1./Vol*d1; // constraint's gradient
  return retF;
}
"""


codigo['volume_gradient_ipopt_conic'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
  real[int] dc = dconsres(0,Wh);
  real[int] d1 = A*dc;

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Vol*d1; // constraint's gradient
  retF = aretF;
  return retF;
}
"""




codigo['volume_gradient_mma_hemholz'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){

  
  real[int] dc = dconsres(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  

  retF(0,:) = 1./Vol*d3; // constraint's gradient
 
  return retF;
}
"""



codigo['volume_gradient_ipopt_hemholz'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation  
  
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);  
  real[int] dc = dconsres(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Vol*d3; // constraint's gradient
  retF = aretF;
  return retF;
}
"""












# -------------------------------------------------------

# NORMALIZATION CONSTANT

# computation of initial compliance for normalization constant
codigo['compliance_normalization'] = """
// Computation of compliance for normalization

rhobar = Volfrac;
Lame;
{compliance}
real Mobj = abs(1./obj);

"""


# -----------------------------------------------------------

# FOR VOLUME PROBLEM

# computation of maximum compliance for volume problem
codigo['compliance_constant'] = """
// Computation of maximum compliance

rhobar = 1.;
Lame;
{compliance}
{comfrac}
cout << "Best compliance: " << compobj << endl;
cout << "Maximum compliance allowed: " << Compfrac << endl;
"""


# volume objective function 

codigo['objective_volume_mma'] = """

// macro function with objective
func real J(real[int] & xx){{

{funcional}

  cout << "Func. evaluation: " << ++jj << "  Objective: " << obj << " " ; 

{plot}

  return obj;
}}
"""

codigo['objective_volume_ipopt_conic'] = """

// macro function with objective
func real J(real[int] & xx){{

    rho[] = xx;
    rhohat[] = A*rho[]; // filter 
    rhobar = tb + tbi*tanh(beta*(rhohat-eta));    
{funcional}

  cout << "Func. evaluation: " << ++jj << "  Objective: " << obj << " " ; 

{plot}

  return obj;
}}
"""

codigo['objective_volume_ipopt_hemholz'] = """

// macro function with objective
func real J(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
{funcional}

  cout << "Func. evaluation: " << ++jj << "  Objective: " << obj << " " ; 

{plot}

  return obj;
}}
"""



# compliance constraint function

codigo['compliance_constraint_mma'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

        
  {funcional}
  real compl = 1./Compfrac*complobj - 1.;
  rF[0] = compl;

  cout << " Restr: " << compl << " (" << complobj << ")" <<endl;  
  return rF;
}}
"""


codigo['compliance_constraint_ipopt_conic'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{
    rho[] = xx;
    rhohat[] = A*rho[]; // filter 
    rhobar = tb + tbi*tanh(beta*(rhohat-eta));    
    Lame;        
  {funcional}
  real compl = 1./Compfrac*complobj - 1.;
  rF[0] = compl;

  cout << " Restr: " << compl << " (" << complobj << ")" <<endl;  
  return rF;
}}
"""


codigo['compliance_constraint_ipopt_hemholz'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{
  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;        
  {funcional}
  real compl = 1./Compfrac*complobj - 1.;
  rF[0] = compl;

 cout << " Restr: " << compl << " (" << complobj << ")" <<endl;  
  return rF;
}}
"""





# gradient for compliance in volume problem
codigo['objective_volume_gradient_mma_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));
  Lame;
  {adj}
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
  real[int] c=dconsres(0,Wh); // objective's gradient
  real[int] d = A*c;
  retJ = 1./medida*d;
  
  return retJ;

}}
"""

codigo['objective_volume_gradient_mma_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;
  {adj}
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
  real[int] c=dconsres(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] d = B'*c2;
  retJ = 1./medida*d;

  return retJ;
}}
"""


codigo['objective_volume_gradient_ipopt_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  {adj}
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
  real[int] c=dconsres(0,Wh); // objective's gradient
  real[int] d = A*c;
  retJ = 1./medida*d;
  
  return retJ;

}}
"""

codigo['objective_volume_gradient_ipopt_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  {adj}
  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
  real[int] c=dconsres(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] d = B'*c2;
  retJ = 1./medida*d;

  return retJ;
}}
"""




# compliance gradient (for volume problem)
codigo['compliance_gradient_mma_conic'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){


  real[int] dc = der(0,Wh);
  real[int] d1 = A*dc;

  retF(0,:) = 1./Compfrac*d1; // constraint's gradient
  return retF;
}
"""

codigo['compliance_gradient_mma_hemholz'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){


  real[int] dc = der(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  

  retF(0,:) = 1./Compfrac*d3; // constraint's gradient
 
  return retF;
}
"""

codigo['compliance_gradient_ipopt_conic'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  rhohat[] = A*rho[]; // filter 
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));
  Lame;

  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
  real[int] dc = der(0,Wh);
  real[int] d1 = A*dc;

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Compfrac*d1; // constraint's gradient
  retF = aretF;
  return retF;
}
"""


codigo['compliance_gradient_ipopt_hemholz'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation  
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
  Lame;


  // compute gradient
  psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);

  real[int] dc = der(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  
  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Compfrac*d3; // constraint's gradient
  retF = aretF;
  return retF;
}
"""
# -------------------------------------------------------




# initializations

codigo['initialization'] = """

//********************************
// INITIALIZATION
//********************************

Xmin=0.0;
Xmax=1.;

rho = Initcte;
"""


codigo['passive_initialization'] = """

//********************************
// INITIALIZATION
//********************************

{pasivo}
varf totalmesh(unused,v) = {integral}(Th)(v);

real[int] vector1 = zonapasiva(0,Wh);
real[int] vector2 = totalmesh(0,Wh);

// set data constraints for MMA
Xmax=1.;
Xmin = vector1./vector2;

// initial rho

Wh auxiliar;
auxiliar[] = Xmin;
rho = max(auxiliar,Initcte);
"""

codigo['init'] = """

//********************************
// INITIALIZATION
//********************************

rho = Initcte;
real[int] rF(1);
int jj;
"""


codigo['force_initialization'] = '\nrho[] = readsol("{name}");\n'

codigo['random_initialization'] = """

gslrng generator = gslrngtype(1);// chose a generator 
//rho = gslrnguniform(generator) ;
for (int i=0;i<Wh.nt;i++) rho[][i] = gslranbeta(generator,Initcte,1-Initcte);
"""

codigo['passive_random_initialization'] = """

//********************************
// INITIALIZATION
//********************************

{pasivo}
varf totalmesh(unused,v) = {integral}(Th)(v);

real[int] vector1 = zonapasiva(0,Wh);
real[int] vector2 = totalmesh(0,Wh);

// set data constraints for MMA
Xmax=1.;
Xmin = vector1./vector2;

// initial rho

gslrng generator = gslrngtype(1);// chose a generator 

Wh auxiliar,aux;
auxiliar[] = Xmin;
//Wh aux = gslrnguniform(generator) ;
for (int i=0;i<Wh.nt;i++) aux[][i] = gslranbeta(generator,Initcte,1-Initcte);
rho = max(auxiliar,aux);
"""


codigo['passive_force_initialization'] = """

{pasivo}
varf totalmesh(unused,v) = {integral}(Th)(v);

real[int] vector1 = zonapasiva(0,Wh);
real[int] vector2 = totalmesh(0,Wh);

// set data constraints for MMA
Xmax=1.;
Xmin = vector1./vector2;

rho[] = readsol("{name}");


"""

# previous for loop for beta update for MMA

codigo['prebeta_mma_conic'] = """

//********************************
// MMA - BETA LOOPS
//********************************

Xini = rho[];

for (int k=0; k<loops; k++){

  // previous evaluation for F
  
  rhohat[] = A*rho[]; // filter
  rhobar = tb + tbi*tanh(beta*(rhohat-eta));
"""


codigo['prebeta_ipopt'] = """

//********************************
// IPOPT - BETA LOOPS
//********************************

Shand[0] = 0.;
Xini = rho[];

for (int k=0; k<loops; k++){


"""



codigo['prebeta_mma_hemholz'] = """

//********************************
// MMA - BETA LOOPS
//********************************

Xini = rho[];

for (int k=0; k<loops; k++){

  // previous evaluation for F
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhosharp = rhohat; // P1 -> P0 interpolation
  rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
"""


# beta loop for MMA

codigo['beta_loops_mma'] = """
  real mini = nloptMMA(J,Xini,grad=dJ,lb=Xmin,ub=Xmax,IConst=F,gradIConst=dF,
                    stopMaxFEval=maxiter,stopAbsFTol=starttol);

  
  rho[] = Xini;
  beta *= betaupdate;
  tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
//  tb = tanh(beta*eta)*tbi;
  cout << " " << endl;
  cout << "Beta changed: " << beta << endl; 
}
"""



# beta loop for MMA

codigo['beta_loops_ipopt'] = """
   IPOPT(J,dJ,F,dF,Xini,lb=Xmin,ub=Xmax,cub=Shand,bfgs=1,maxiter=maxiter,printlevel=0,tol=starttol);

  
  rho[] = Xini;
  beta *= betaupdate;
  tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
//  tb = tanh(beta*eta)*tbi;
  cout << " " << endl;
  cout << "Beta changed: " << beta << endl; 
}
"""



# final updates

codigo['final_update_conic'] = """

//********************************
// FINAL UPDATE
//********************************

{beta}
tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
// tb = tanh(beta*eta)*tbi;
cout << "Beta final: " << beta << endl; 
rhohat[] = A*rho[]; // filter 
rhobar = tb + tbi*tanh(beta*(rhohat-eta));
Lame;

real fobj = J(rho[]); // objective

cout << "FIN -- Objective: " << fobj << endl;

jj++;

{plotd}
"""

codigo['final_update_hemholz'] = """

//********************************
// FINAL UPDATE
//********************************

{beta}
tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
// tb = tanh(beta*eta)*tbi;
cout << "Beta final: " << beta << endl; 
real[int] rb = filter(0,Zh); // second hand of filter problem
rhohat[] = A^-1*rb; // solves filter problem
rhosharp = rhohat; // P1 -> P0 interpolation
rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
Lame;

real fobj = J(rho[]); // objective

cout << "FIN -- Objective: " << fobj << endl;

jj++;

{plotd}
"""

codigo['final'] = """
cout << "Total Time: " << clock() - time1 << endl;
cout << "PROCESS ENDED" << endl;

"""


# loop for optimality conditions

codigo['optimality_conic'] = """
//********************************
// ITERATIONS
//********************************
int k=0, loopbeta = 1;
real change = 1.;

while (k<maxiter && change > starttol )
 {{ 
    k++;
    //compute filtering and displacement
    rhohat[] = A*rho[]; // conic filter 
    rhobar = tb + tbi*tanh(beta*(rhohat-eta));
    Lame;

    J(rho[]);
    F(rho[]);
    

    // compute gradient
    psip = beta*tbi*(1-tanh(beta*(rhohat-eta))^2);
    real[int] c=der(0,Wh); // variational form of objective's gradient
    Wh bk;
    bk[] = A*c; // conic filter
    

    // constraint's gradient
    real[int] dr = dconsres(0,Wh);
    Wh dc;
    dc[] = A*dr; // conic filter
    
    // bisection method to adjust volume constraint
    real l1=0, l2=1.e9, lmid;
    Wh rbk = rho*(max(0.,-Mobj*bk/dc))^neta;
    Wh r1 = rho*(1+zeta);
    Wh r2 = rho*(1-zeta);
    while ( (l2-l1)/(l1+l2) > 1.e-3) 
     {{   
       lmid = 0.5*(l1+l2);
       Wh rl = rbk/lmid^neta;
       Wh aux1 = min(r1,rl);
       Wh aux2 = min(1.,aux1);
       Wh aux3 = max(r2,aux2);
       Rk = max(0.,aux3);

       Wh Rhohat;       
       Rhohat[] = A*Rk[]; // conic filter
       Wh Rhobar = tb + tbi*tanh(beta*(Rhohat-eta));
        
       {volrhobar}
       if (vol > Vol) {{l1 = lmid;}} else {{l2 = lmid;}}
     }}
    // update
    real[int] diff= Rk[]-rho[];
    change = diff.linfty;
    rho = Rk;

    // beta update
    if ( (loopbeta < loops && k >= maxiter) || change < starttol) 
      {{
       beta *= betaupdate;
       tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
       // tb = tanh(beta*eta)*tbi;
       loopbeta++;
       change = 1.;
       cout << " " << endl;
       cout << "Beta changed: " << beta << endl; 
       k = 0;
      }}
 }}
"""      

codigo['optimality_hemholz'] = """
//********************************
// ITERATIONS
//********************************
int k=0, loopbeta = 1;
real change = 1.;

while (k<maxiter && change > starttol )
 {{ 
    k++;
    //compute filtering and displacement
    real[int] rb = filter(0,Zh); // second hand of filter problem
    rhohat[] = A^-1*rb; // solves filter problem
    rhosharp = rhohat; // P1 -> P0 interpolation
    rhobar = tb + tbi*tanh(beta*(rhosharp-eta));
    Lame;

    J(rho[]);
    F(rho[]);

    // compute gradient
    psip = beta*tbi*(1-tanh(beta*(rhosharp-eta))^2);
    real[int] c=der(0,Wh); // variational form of objective's gradient
    real[int] c1 = PI'*c;
    real[int] c2 = A^-1*c1;
    Wh bk;
    bk[] = B'*c2; 

    // constraint's gradient
    real[int] dr = dconsres(0,Wh);
    real[int] d1 = PI'*dr;
    real[int] d2 = A^-1*d1;
    Wh dc;
    dc[] = B'*d2;
    
    // bisection method to adjust volume constraint
    real l1=0, l2=1.e9, lmid;
    Wh rbk = rho*(max(0.,-Mobj*bk/dc))^neta;
    Wh r1 = rho*(1+zeta);
    Wh r2 = rho*(1-zeta);
    while ( (l2-l1)/(l1+l2) > 1.e-3) 
     {{
       lmid = 0.5*(l1+l2);
       Wh rl = rbk/lmid^neta;
       Wh aux1 = min(r1,rl);
       Wh aux2 = min(1.,aux1);
       Wh aux3 = max(r2,aux2);
       Rk = max(0.,aux3);

       real[int] rbi = filterk(0,Zh);
       Zh Rhohat;
       Rhohat[] = A^-1*rbi;       
       Wh Rhosharp = Rhohat; // P1 -> P0 interpolation
       Wh Rhobar = tb + tbi*tanh(beta*(Rhosharp-eta));
        
       {volrhobar}
       if (vol > Vol) {{l1 = lmid;}} else {{l2 = lmid;}}
      
      }}
    // update
	real[int] diff= Rk[]-rho[];
    change = diff.linfty;
    rho = Rk;

    // beta update
    if ( (loopbeta < loops && k >= maxiter) || change < starttol) 
      {{
       beta *= betaupdate;
	   tbi = 1./(tanh(beta*eta) + tanh(beta*(1-eta)));
	   // tb = tanh(beta*eta)*tbi;
       loopbeta++;
       change = 1.;
       cout << " " << endl;
       cout << "Beta changed: " << beta << endl; 
       k = 0;
      }}
 }}
"""


codigo['vonmisses_2d'] = """
macro sigma(u) [E(rhobar)*(lambda1*div(u)+mu1*dx(u#1)),
                E(rhobar)*(lambda1*div(u)+mu1*dy(u#2)),
                E(rhobar)*mu1/2.*(dx(u#2) + dy(u#1))] // EOM

macro vm(u) sqrt( u[0]^2 + u[1]^2 - u[0]*u[1] + 3*u[2]^2 ) // EOM

Wh stressvm = vm(sigma(u));
"""



codigo['vonmisses_3d'] = """
macro sigma(u) [E(rhobar)*(lambda1*div(u)+mu1*dx(u#1)),
                E(rhobar)*(lambda1*div(u)+mu1*dy(u#2)),
                E(rhobar)*(lambda1*div(u)+mu1*dz(u#3)),
                E(rhobar)*mu1/2.*(dx(u#2) + dy(u#1)),
                E(rhobar)*mu1/2.*(dy(u#3) + dz(u#2)),
                E(rhobar)*mu1/2.*(dx(u#3) + dz(u#1))] // EOM

macro vm(u) sqrt( u[0]^2 + u[1]^2 + u[2]^2 - u[0]*u[1] 
           - u[1]*u[2] - u[2]*u[0] 
           + 3*(u[3]^2 + u[4]^2 + u[5]^2) ) // EOM

Wh stressvm = vm(sigma(u));
"""

codigo['savenumeric'] = """
savesol("{resultadopure}",Th,rho,order=0);
savesol("{resultadofilter}",Th,rhobar,order=0);

"""



codigo['threshold'] =  """
int ndivisiones = 40;
real[int] xabcisa(ndivisiones+1), rhordenada(ndivisiones+1);
real cpaso = 1./ndivisiones, auxd;
for (int i = 0; i<=ndivisiones; i++){{
	auxd = i*cpaso;
	xabcisa[i] = auxd;
	rhordenada[i] =1./medida*{integral}(Th)((rhobar>=auxd));
}}


func real fff(real d){{
	return 1./medida*{integral}(Th)(rhobar >= d) - Volfrac;
}}

func real biseccion(real &a,real &b){{
        if (fff(a) < 1.e-4) return a;
	    real medio = (b+a)/2.;

 	 	 while (abs(fff(medio))>1.e-4)
 	 	 {{
	 	 if ( fff(medio) > 0 ) a = medio;
 	 	 else b=medio;
 	 	 medio = (b+a)/2.;
 	     }}
 	    return medio;
}}


real extinf,extsup,threshold;
for (int i = 0; i<ndivisiones; i++){{
	if ( (rhordenada[i] >= Volfrac) && (rhordenada[i+1] < Volfrac) )
	{{
		extinf = xabcisa[i];
		extsup = xabcisa[i+1];
		threshold = biseccion(extinf,extsup);
		break;
	}}
}}

cout << "--------------------------------" << endl;
cout << "-------- Threshold: " << threshold << endl;
cout << "--------------------------------" << endl;


// dump data to file

    
string doc1 = "# vtk DataFile Version 4.1\\
Generated by FreeFem++\\
ASCII\\
DATASET TABLE\\
ROW_DATA ";
{{
ofstream gnu("{dibujo}");
{{
gnu << doc1 << 3*(ndivisiones+1)+1 << endl;
gnu << "FIELD FieldData 4"<< endl;
gnu << "X 1 " << ndivisiones+1 << " float" << endl;
for (int i=0;i<=ndivisiones;i++)
	gnu << xabcisa[i] << "  " ;
gnu << endl;
gnu << "Volume 1 " << ndivisiones+1 << " float" << endl; 
for (int i=0;i<=ndivisiones;i++)
    gnu << rhordenada[i] << "  ";   
gnu << endl;
gnu << "Fraction%20Volume 1 " << ndivisiones+1 << " float" << endl; 
for (int i=0;i<=ndivisiones;i++)
    gnu << Volfrac << "  ";   
gnu << endl;
gnu << "Threshold 1 1 float" << endl;
gnu << threshold << endl;
}}
}}

{{
ofstream gnu("{dibujo}.table");
gnu << "x        y1      y2    " << threshold <<  endl;
for (int i=0;i<=ndivisiones;i++)
	gnu << xabcisa[i] << "  " << rhordenada[i] << "  " << Volfrac << "  " << "nan" << endl;
}}



"""






#codigo['savenumeric'] = """
#savesol("{resultado}",Th,rhobar,order=0);
#
#"""


codigo['dummy'] = ''


plot = 'plot(rhobar,fill=1,hsv=colormapHSVgray,viso=visoHSVgray,cmm="Iteration: "+jj+": "+obj);'
#plotvtk = 'savevtk(name+jiter+".vtk",Th,rhobar,dataname="density"); '

plotd2 = 'int[int] ord=[0,0,1]; \nsavevtk("{name}.vtk",Th,rhobar,stressvm,[u1,u2,0],order=ord,dataname="density stress displacements",floatsol=0);'
plotd3 = 'int[int] ord=[0,0,1]; \nsavevtk("{name}.vtk",Th,rhobar,stressvm,[u1,u2,u3],order=ord,dataname="density stress displacements",floatsol=0);'


main_names = {}
main_names['previous'] = 'previous'
main_names['mesh'] = 'mesh'
main_names['parameters'] = 'parameters'
main_names['spaces'] = ''
main_names['func_definition'] = 'func_definition'
main_names['elasticity_system'] = 'elasticity_system'
main_names['acting_forces'] = 'acting_forces'
main_names['clamped_conditions'] = 'clamped_conditions'
main_names['adjoint'] = 'adjoint'
main_names['variational_derivatives'] = 'variational_derivatives'
main_names['sin_beta'] = 'sin_beta'
main_names['constant_compliance'] = 'compliance_normalization'
main_names['filtro'] = 'filter_'
main_names['mma_variables'] = 'mma_variables'
main_names['objective'] = 'objective'
main_names['volume_constraint'] = 'volume_constraint'
main_names['compliance_constraint'] = 'compliance_constraint'
main_names['objective_gradient'] = 'objective_gradient_'
main_names['volume_gradient'] = 'volume_gradient_'
main_names['initialization'] = 'initialization'
main_names['prebeta'] = 'prebeta_'
main_names['beta_loops'] = 'beta_loops'
main_names['final_update'] = 'final_update_'
main_names['iterations'] = 'optimality_'
main_names['stress'] = 'vonmisses_'
main_names['savenumeric'] = 'savenumeric'
main_names['readnumeric'] = 'dummy'
main_names['threshold'] = 'threshold'
main_names['final'] = 'final'

mma = ['previous','mesh','parameters','spaces','func_definition','elasticity_system',
       'acting_forces','clamped_conditions','adjoint','variational_derivatives','filtro','sin_beta','constant_compliance',
       'mma_variables','objective','volume_constraint','objective_gradient',
       'volume_gradient','initialization','readnumeric','prebeta','beta_loops','stress',\
       'final_update','savenumeric','threshold','final']

optimality = mma[:13] + ['initialization','readnumeric','objective','volume_constraint','iterations',\
                'stress','final_update','savenumeric','threshold','final']


adaptacion = """
load "medit";
mesh Th = readmesh("{malla}");

fespace Vh(Th,P0);
Vh denbar,denpur,new;
denbar[] = readsol("{initsol}");
denpur[] = readsol("{initpuresol}");

int act = int({nbvx}*Th.nv);
cout << act << endl;
Th = adaptmesh(Th,denbar,nbvx=act);

new = denpur;
savemesh(Th,"{mallaadaptada}");
savesol("{solucionadaptada}",Th,new,order=0);

cout << "ADAPTATION ENDED" << endl;
"""

