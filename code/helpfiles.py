#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:44:46 2018

@author: earanda
"""

helpers = {}

helpers["Elastic Constants"] = "elastic.png"
helpers["Optimization's Parameters"] = "optimization.png"
helpers["Algorithm's Parameters"] = "algorithm.png"
helpers["Inner Loads"] = "innerloads.png"