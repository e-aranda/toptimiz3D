# -*- coding: utf-8 -*-
"""
Created on Sat Jul 15 19:51:21 2017

@author: earanda

External functions
"""

from subprocess import getstatusoutput
import tempfile

def is_number(s):
    """ Returns True if string is a number. """
    try:
        float(s)
        return True
    except ValueError:
        return False        
    
def checkempty(cad):
    """ Returns True is not empty string """
    if cad.isspace() == True or not cad:
        return False
    else:
        return True

def string2number(s):
    """
    Convert a string to a number (int or float)
    """
    try:
        return int(s)
    except ValueError:
        return float(s)
    
def vtkbuilder(name,vtkf,gmsh):
    """
    Convert FreeFem++ mesh file in vtk format
    """
    texto = 'load "iovtk"; \n{load} \n{malla}\nsavevtk("' + vtkf + '",Th);'

    if gmsh:
        load = 'load "gmsh";'
        malla2 = 'mesh Th = gmshload("' + name + '");'
        malla3 = 'mesh3 Th = gmshload3("' + name + '");'
    else:
        load = ''
        malla2 = 'mesh Th = readmesh("'+ name + '");' 
        malla3 = 'mesh3 Th = readmesh3("'+ name + '");'

    with tempfile.NamedTemporaryFile() as temp:
        temp.write(texto.format(load=load,malla=malla2).encode())
        ejecutar = 'FreeFem++ -ne -v 0 {temporal}'
        temp.flush()
        err,info2 = getstatusoutput(ejecutar.format(temporal=temp.name)) 
    if err:
        with tempfile.NamedTemporaryFile() as temp:
            temp.write(texto.format(load=load,malla=malla3).encode())
            temp.flush()
            err,info3 = getstatusoutput(ejecutar.format(temporal=temp.name)) 
    if not err:
        return err,''        
    else:
        return err,info2 + '\n' + info3
