# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:12:33 2017

@author: earanda

FreeFem codes for topology optimization problem
"""

codigo = {}


# parámetros del problema
codigo['parameters'] = """

//*************************************
// PARAMETERS
//*************************************

// elastic parameters
{young}

real E10 = E1-E0;
// Lamé parameters: 2*mu = mu1*E, lambda = lambda1*E
real mu1 = 1./(1+nu); 
{lambda1}

// SIMP parameter
int p = {penal};

// filter parameter
real rfilter= {filtro};

// maximum of iterations of one call of MMA
int maxiter = {maxiter};

// tolerance (for MMA)
real starttol = {tolerance};

// constant for initialization
real Initcte = {initcte};

// maximum volume allowed
real Volfrac = {volume};
{medida}
real Vol = Volfrac*medida;


{loops}
{betaupdate}
"""



# definiciones de espacios

codigo['spaces_conic_2d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1]); // displacements
fespace Wh(Th,P0); // density

Vh [u1,u2], [v1,v2] {adj};
Wh rho,rhobar,rhohat,Rk;
"""

codigo['spaces_hemholz_2d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1]); // displacements
fespace Wh(Th,P0); // density
fespace Zh(Th,P1); // filter

Vh [u1,u2], [v1,v2] {adj};
Wh rho,rhobar,Rk;
Zh rhohat;
"""


codigo['spaces_conic_3d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1,P1]); // displacements
fespace Wh(Th,P0); // density

Vh [u1,u2,u3], [v1,v2,v3] {adj};
Wh rho,rhobar,rhohat,Rk;
"""

codigo['spaces_hemholz_3d'] = """

//********************************
// SPACES AND FUNCTIONS
//********************************

// finite element spaces
fespace Vh(Th,[P1,P1,P1]); // displacements
fespace Wh(Th,P0); // density
fespace Zh(Th,P1); // filter

Vh [u1,u2,u3], [v1,v2,v3] {adj};
Wh rho,rhobar,Rk;
Zh rhohat;
"""






# derivadas variacionales del compliance y el volumen

codigo['variational_derivatives']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*( mu1*epsilon(u)'*epsilon(u)
+ lambda1*div(u)^2 )*v ) {uu}; 

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(v); 
"""

codigo['variational_derivatives_adj']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*( mu1*epsilon(u)'*epsilon(q)
+ lambda1*div(u)*div(q) )*v ) {uu}; 

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(v); 
"""


codigo['variational_derivatives_weighted']  = """

// variational formulation for objective's gradient
varf der(unused,v) = {integral}(Th)( -E10*p*rhobar^(p-1)*( mu1*epsilon(u)'*epsilon(u)
                     + lambda1*div(u)^2 )*v ) + {integral}(Th)(2*density*{uu}*v);

// variational formulation for constraint's gradient
varf dconsres(unused,v)= {integral}(Th)(v); 
"""





codigo['sin_beta_conic'] = """

  real[int] dc = dconsres(0,Wh);
  real[int] d1 = A*dc;

"""

codigo['sin_beta_hemholz'] = """

  real[int] dc = dconsres(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  

"""






codigo['objective_ipopt_conic'] = """
// macro function with objective
func real J(real[int] & xx){{

    rho[] = xx;
    rhobar[] = A*rho[]; // filter 
    Lame;       

{funcional}
real mobj = Mobj*obj; // normalized cost

  cout << "Func. evaluation: " << ++jj << "  Objective: " << mobj << " (" << obj << ") " ; 

{plot}

  return mobj;
}}
"""

codigo['objective_ipopt_hemholz'] = """
// macro function with objective
func real J(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
  Lame;

{funcional}
real mobj = Mobj*obj; // normalized cost

  cout << "Func. evaluation: " << ++jj << "  Objective: " << mobj << " (" << obj << ") " ; 

{plot}

  return mobj;
}}
"""





codigo['volume_constraint_ipopt_conic'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

  rho[] = xx;    
  rhobar[] = A*rho[]; // filter 
  
  real volumen = 1./Vol*{integral}(Th)(rhobar)-1.; //normalized volume constraint
  rF[0] = volumen;
  cout << " Restr: " << volumen << endl;
  
  return rF;
}}
"""

codigo['volume_constraint_ipopt_hemholz'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation

  real volumen = 1./Vol*{integral}(Th)(rhobar)-1.; //normalized volume constraint
  rF[0] = volumen;
  cout << " Restr: " << volumen << endl;
  
  return rF;
}}
"""





# objective compliance gradient

codigo['objective_gradient_mma_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  Lame;
  
  {adj}
 
        
  // compute gradient
  real[int] c=der(0,Wh); // objective's gradient
  real[int] retaJ = A*c;
  retJ = Mobj*retaJ;
  
  return retJ;

}}
"""

codigo['objective_gradient_ipopt_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  Lame;
  
  {adj}
        
  // compute gradient
  real[int] c=der(0,Wh); // objective's gradient
  real[int] retaJ = A*c;
  retJ = Mobj*retaJ;
  
  return retJ;

}}
"""




codigo['objective_gradient_mma_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
  Lame;

  {adj}
  
  
  // compute gradient
  real[int] c=der(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] retaJ = B'*c2;
  retJ = Mobj*retaJ;

  return retJ;
}}
"""





codigo['objective_gradient_ipopt_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation  
  Lame;

  {adj}

  // compute gradient
  real[int] c=der(0,Wh); // objective's gradient
  real[int] c1 = PI'*c;
  real[int] c2 = A^-1*c1;
  real[int] retaJ = B'*c2;
  retJ = Mobj*retaJ;

  return retJ;
}}
"""

# volume constraint gradient

codigo['volume_gradient_mma_conic'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){

  retF(0,:) = 1./Vol*d1; // constraint's gradient
  return retF;
}
"""


codigo['volume_gradient_ipopt_conic'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  // compute gradient

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Vol*d1; // constraint's gradient
  retF = aretF;
  return retF;
}
"""




codigo['volume_gradient_mma_hemholz'] = """
// constraint's gradient
func real[int,int] dF(real[int] & xx){

  retF(0,:) = 1./Vol*d3; // constraint's gradient
  return retF;
}
"""



codigo['volume_gradient_ipopt_hemholz'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation  
  
  // compute gradient

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Vol*d3; // constraint's gradient
  retF = aretF;
  return retF;
}
"""




# -----------------------------------------------------------

# FOR VOLUME PROBLEM


# volume objective function 


codigo['objective_volume_ipopt_conic'] = """

// macro function with objective
func real J(real[int] & xx){{

    rho[] = xx;
    rhobar[] = A*rho[]; // filter 
{funcional}

  cout << "Func. evaluation: " << ++jj << "  Objective: " << obj << " " ; 

{plot}

  return obj;
}}
"""

codigo['objective_volume_ipopt_hemholz'] = """

// macro function with objective
func real J(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
{funcional}

  cout << "Func. evaluation: " << ++jj << "  Objective: " << obj << " " ; 

{plot}

  return obj;
}}
"""





codigo['compliance_constraint_ipopt_conic'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{
    rho[] = xx;
    rhobar[] = A*rho[]; // filter 
    Lame;        
  {funcional}
  real compl = 1./Compfrac*complobj - 1.;
  rF[0] = compl;

 cout << " Restr: " << compl << " (" << complobj << ")" <<endl;  
  return rF;
}}
"""


codigo['compliance_constraint_ipopt_hemholz'] = """
// macro function with constraints
func real[int] F(real[int] & xx){{
  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
  Lame;        
  {funcional}
  real compl = 1./Compfrac*complobj - 1.;
  rF[0] = compl;

  cout << " Restr: " << compl << " (" << complobj << ")" <<endl;  
  return rF;
}}
"""





# gradient for compliance in volume problem
codigo['objective_volume_gradient_mma_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  Lame;
  {adj}
  // compute gradient

  retJ = 1./medida*d1;
  
  return retJ;

}}
"""

codigo['objective_volume_gradient_mma_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation

  Lame;
  {adj}
  // compute gradient

  retJ = 1./medida*d3;

  return retJ;
}}
"""


codigo['objective_volume_gradient_ipopt_conic'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  {adj}
  // compute gradient

  retJ = 1./medida*d1;
  
  return retJ;

}}
"""

codigo['objective_volume_gradient_ipopt_hemholz'] = """
// macro function with objective's gradient

func real[int] dJ(real[int] & xx){{

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
  {adj}
  // compute gradient

  retJ = 1./medida*d3;

  return retJ;
}}
"""



codigo['compliance_gradient_ipopt_conic'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  rhobar[] = A*rho[]; // filter 
  Lame;

  real[int] dc = der(0,Wh);
  real[int] d1 = A*dc;

  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Compfrac*d1; // constraint's gradient
  retF = aretF;
  return retF;
}
"""


codigo['compliance_gradient_ipopt_hemholz'] = """
// constraint's gradient
func matrix dF(real[int] & xx){

  rho[] = xx;
  real[int] rb = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rb; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation  
  Lame;


  // compute gradient

  real[int] dc = der(0,Wh);
  real[int] d1 = PI'*dc;
  real[int] d2 = A^-1*d1;
  real[int] d3 = B'*d2;  
  real[int,int] aretF(nc,nv);
  aretF(0,:) = 1./Compfrac*d3; // constraint's gradient
  retF = aretF;
  return retF;
}
"""
# -------------------------------------------------------







# previous for loop for beta update for MMA

codigo['prebeta_mma_conic'] = """


Xini = rho[];

// previous evaluation for F
  
  rhobar[] = A*rho[]; // filter
"""


codigo['prebeta_ipopt'] = """

//********************************
// IPOPT - BETA LOOPS
//********************************

Shand[0] = 0.;
Xini = rho[];


"""



codigo['prebeta_mma_hemholz'] = """

//********************************
// MMA - BETA LOOPS
//********************************

Xini = rho[];


  // previous evaluation for F
  real[int] rob = filter(0,Zh); // second hand of filter problem
  rhohat[] = A^-1*rob; // solves filter problem
  rhobar = rhohat; // P1 -> P0 interpolation
"""


# beta loop for MMA

codigo['beta_loops_mma'] = """
  real mini = nloptMMA(J,Xini,grad=dJ,lb=Xmin,ub=Xmax,IConst=F,gradIConst=dF,
                    stopMaxFEval=maxiter,stopAbsFTol=starttol);

  
  rho[] = Xini;

"""



# beta loop for MMA

codigo['beta_loops_ipopt'] = """
   IPOPT(J,dJ,F,dF,Xini,lb=Xmin,ub=Xmax,cub=Shand,bfgs=1,maxiter=maxiter,printlevel=0,tol=starttol);

  
  rho[] = Xini;

"""



# final updates

codigo['final_update_conic'] = """


//********************************
// FINAL UPDATE
//********************************

{beta}
rhobar[] = A*rho[]; // filter 

Lame;

real fobj = J(rho[]); // objective

cout << "FIN -- Objective: " << fobj << endl;

jj++;


{plotd}
"""

codigo['final_update_hemholz'] = """

//********************************
// FINAL UPDATE
//********************************

{beta}
real[int] rb = filter(0,Zh); // second hand of filter problem
rhohat[] = A^-1*rb; // solves filter problem
rhobar = rhohat; // P1 -> P0 interpolation
Lame;

real fobj = J(rho[]); // objective

cout << "FIN -- Objective: " << fobj << endl;

jj++;


{plotd}
"""



# loop for optimality conditions

codigo['optimality_conic'] = """
//********************************
// ITERATIONS
//********************************
int k=0;
real change = 1.;
Wh dic;
dic[] = d1;    

while (k<maxiter && change > starttol )
 {{ 
    k++;
    //compute filtering and displacement
    rhobar[] = A*rho[]; // conic filter 

    Lame;

    J(rho[]);
    F(rho[]);
    

    // compute gradient
    real[int] c=der(0,Wh); // variational form of objective's gradient
    Wh bk;
    bk[] = A*c; // conic filter

    // constraint's gradient


    // bisection method to adjust volume constraint
    real l1=0, l2=1.e9, lmid;
    Wh rbk = rho*(max(0.,-Mobj*bk/dic))^neta;
    Wh r1 = rho*(1+zeta);
    Wh r2 = rho*(1-zeta);
    while ( (l2-l1)/(l1+l2) > 1.e-3) 
     {{   
       lmid = 0.5*(l1+l2);
       Wh rl = rbk/lmid^neta;
       Wh aux1 = min(r1,rl);
       Wh aux2 = min(1.,aux1);
       Wh aux3 = max(r2,aux2);
       Rk = max(0.,aux3);

       Wh Rhobar;       
       Rhobar[] = A*Rk[]; // conic filter
        
       {volrhobar}
       if (vol > Vol) {{l1 = lmid;}} else {{l2 = lmid;}}
     }}
    // update
    real[int] diff= Rk[]-rho[];
    change = diff.linfty;
    rho = Rk;

 }}
"""      

codigo['optimality_hemholz'] = """
//********************************
// ITERATIONS
//********************************
int k=0;
real change = 1.;
Wh dic;
dic[] = d3;    
while (k<maxiter && change > starttol )
 {{ 
    k++;
    //compute filtering and displacement
    real[int] rb = filter(0,Zh); // second hand of filter problem
    rhohat[] = A^-1*rb; // solves filter problem
    rhobar = rhohat; // P1 -> P0 interpolation
    Lame;

    J(rho[]);
    F(rho[]);

    // compute gradient
    real[int] c=der(0,Wh); // variational form of objective's gradient
    real[int] c1 = PI'*c;
    real[int] c2 = A^-1*c1;
    Wh bk;
    bk[] = B'*c2; 

    // constraint's gradient
    
    // bisection method to adjust volume constraint
    real l1=0, l2=1.e9, lmid;
    Wh rbk = rho*(max(0.,-Mobj*bk/dic))^neta;
    Wh r1 = rho*(1+zeta);
    Wh r2 = rho*(1-zeta);
    while ( (l2-l1)/(l1+l2) > 1.e-3) 
     {{
       lmid = 0.5*(l1+l2);
       Wh rl = rbk/lmid^neta;
       Wh aux1 = min(r1,rl);
       Wh aux2 = min(1.,aux1);
       Wh aux3 = max(r2,aux2);
       Rk = max(0.,aux3);

       real[int] rbi = filterk(0,Zh);
       Zh Rhohat;
       Rhohat[] = A^-1*rbi;       
       Wh Rhobar = Rhohat; // P1 -> P0 interpolation
        
       {volrhobar}
       if (vol > Vol) {{l1 = lmid;}} else {{l2 = lmid;}}
      
      }}
    // update
	real[int] diff= Rk[]-rho[];
    change = diff.linfty;
    rho = Rk;

 }}
"""
