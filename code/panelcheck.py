# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:57:09 2017

@author: earanda
"""

import wx
import panelentry
import paneltitle


# ----------------------------------------------------------------------------
class MyPanelCheckSimple(panelentry.MyPanelEntry):
    """
    Panel containing tickers and label entries for individual entries
    """
    def __init__(self,parent,padre,title,varname,panelname,objname):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        self.names = [objname]
        self.conversor = lambda x: x
        
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        # creating a panel with tick and entry for label with information
        panel = wx.Panel(self,style=wx.BORDER_SUNKEN)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.tickers = wx.CheckBox(panel, -1, varname)
        self.labels = wx.TextCtrl(panel,-1,'',style=wx.TE_PROCESS_ENTER|wx.TE_RICH)
        self.labels.Enable(False)
        newlabel = wx.StaticText(panel,-1,panelname)
        sizer.Add(self.tickers,1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
        sizer.Add(self.labels,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 0)
        sizer.Add(newlabel,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 0)
        m_sizer.Add(panel,0, wx.TOP|wx.EXPAND , 1)
        panel.SetSizer(sizer)
        #bindings
        self.Bind(wx.EVT_CHECKBOX,self.OnTextCtrl,self.tickers)
        self.Bind(wx.EVT_TEXT, lambda event, ind=0: self.set_val(event,ind), self.labels)
        
        self.SetSizer(m_sizer)
            
    def OnTextCtrl(self,evt):
        """
        Activate/Deactive label entry depending on ticking
        and clearing values in MainFrame
        Connected to MainFrame
        """
        if self.tickers.Value:
            self.labels.Enable(True)
            self.padre.values[self.names[0] + '_tick'] = True
        else:
            self.labels.Enable(False)
            self.labels.SetValue("")
            self.padre.values[self.names[0]] = ''
            self.padre.values[self.names[0] + '_tick']  = False
        # unblock OC 
        self.padre.Validating()
            

        

class MyPanelCheck(panelentry.MyPanelEntry):
    """
    Panel containing tickers and label entries in dictionary form
    """
    def __init__(self,parent,padre,title,varname,objname):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        self.name = objname
        
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        paneles = {}
        sizers = {}
        self.tickers = {}
        self.labels = {}
        
        # panel with information line
        title = wx.Panel(self)
        t_sizer = wx.BoxSizer(wx.HORIZONTAL)
        t1 = wx.StaticText(title,-1,"Marked Options")
        t2 = wx.StaticText(title,-1,"Labels")
        t_sizer.Add(t1,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
        t_sizer.Add(t2,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        title.SetSizer(t_sizer)
        
        # creating a panel in each line with tick and entry for label
        for i in varname:
            paneles[i] = wx.Panel(self,style=wx.BORDER_SUNKEN)
            sizers[i] = wx.BoxSizer(wx.HORIZONTAL)
            self.tickers[i] = wx.CheckBox(paneles[i], -1, i)
            self.labels[i] = wx.TextCtrl(paneles[i],-1,'',style=wx.TE_PROCESS_ENTER|wx.TE_RICH)
            self.labels[i].Enable(False)
            sizers[i].Add(self.tickers[i],1,wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL,1)
            sizers[i].Add(self.labels[i],2,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 0)
            m_sizer.Add(paneles[i],0, wx.TOP|wx.EXPAND , 1)
            paneles[i].SetSizer(sizers[i])
            # bindins
            self.Bind(wx.EVT_CHECKBOX,lambda event, ind=i: self.OnTextCtrl(event,ind), self.tickers[i])
            self.Bind(wx.EVT_TEXT, lambda event, ind=i: self.set_val(event,ind), self.labels[i])
        self.SetSizer(m_sizer)
        
    def OnTextCtrl(self,evt,i):
        """
        Overloaded method
        Activate/Deactive label entry depending on ticking
        and clearing values in MainFrame
        Connected to MainFrame
        """
        if self.tickers[i].Value:
            self.labels[i].Enable(True)
            self.padre.values[self.name+'_tick'][i] = True
        else:
            self.labels[i].Enable(False)
            self.labels[i].SetValue("")
            if i in self.padre.values[self.name].keys():
                self.padre.values[self.name][i] = ''
                self.padre.values[self.name+'_tick'][i] = False

 
       
    def set_val(self,event,i):
        """
        Overloaded method
        Event for edition of data entries. Set values in MainFrame
        Connected to MainFrame
        """
        self.padre.values[self.name][i] = event.EventObject.GetValue()


    def set_dim(self,dim):
        """
        Change access to 3rd coordinate in case of changing dim
        """
        if dim == 2:
            for x in ['Z','XZ','YZ','XYZ']:
                self.labels[x].SetValue('')
                self.tickers[x].Enable(False)
        else:
            for x in ['Z','XZ','YZ','XYZ']:  
                self.tickers[x].Enable(True)
