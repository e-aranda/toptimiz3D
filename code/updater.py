#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 10:13:17 2018

@author: earanda
"""

import requests
import os
import wx
import zipfile
import tempfile
import shutil

RAMA = 'master'
RAMANAME = 'Develop version'
PYTHON = 'python3 '

def check():
    script_dir = os.path.dirname(os.path.abspath(__file__))
    myfile = os.path.join(script_dir,'../VERSION')
    url = 'https://gitlab.com/e-aranda/toptimiz3D/raw/' + RAMA + '/VERSION'
    r = requests.get(url)
    ver = r.text.strip()
    nver = (ver.split('/')[0]).strip()
    cver = (ver.split('/')[1]).strip()
    with open(os.path.realpath(myfile),'r') as f:
        x = f.readline().strip()
    y = (x.split('/')[0]).strip()
    if float(y) >= float(nver):
        return True,True
    elif float(y) >= float(cver):
        return False,True
    else:
        return False,False
    

def download():
    filename = "toptimiz3D-" + RAMA + '.zip'
    url = "https://gitlab.com/e-aranda/toptimiz3D/-/archive/"  + RAMA + '/' + filename

    try:
        r = requests.get(url)
    except IOError as e:
        print("Can't retrieve file")
        return    
    tmpdir = tempfile.mkdtemp()
    filezip = os.path.join(tmpdir,filename)
    open(filezip,'wb').write(r.content)
    try:
        z = zipfile.ZipFile(filezip)
    except zipfile.error as e:
        print("Bad zipfile (from {0}): {1}".format(url, e))
        return
 
    z.extractall(tmpdir)
    dirname = z.namelist()[0]
    z.close()   
    
    return tmpdir,dirname


def instalador(origen,folder,padre):
 
    # Copy VERSION and LICENSE files
    for x in ('LICENSE','VERSION'):        
        dst = os.path.join(folder,x)
        print("Copying " + x)
        try:
            shutil.copyfile(os.path.join(origen,x),dst)
#            print(os.path.join(origen,x),dst)
        except shutil.SameFileError as e:
            print(e.args)
        except IOError as e:
            raise Exception(e.args)

    for carpet in ['code','examples']:
        origenfolder = os.path.join(origen,carpet)
        codefolder = os.path.join(folder,carpet)                
        
        # Copying Python files and icons
        for x in os.listdir(origenfolder):
            src = os.path.join(origenfolder,x)
            dst = os.path.join(codefolder,x)
            if not os.path.isdir(src):
                print("Copying " + x + " ...")
                try:
                    shutil.copyfile(src,dst)
#                    print(src,dst)
                except shutil.SameFileError as e:
                    print(e.args)
                except IOError as e:
                    raise Exception(e.args)
            else:
                print("Enter in " + x + " ...")
                for y in os.listdir(src):
                    print("  Copying " + y + " ...")
                    ini = os.path.join(src,y)
                    dest = os.path.join(dst,y)
                    try:
                        shutil.copyfile(ini,dest)
#                        print(ini,dest)
                    except shutil.SameFileError as e:
                        print(e.args)
                    except IOError as e:
                        raise Exception(e.args)
                        
    padre.Close()
    wx.MessageBox('Updated. Please save your data and rerun application', 'Info',
            wx.OK | wx.ICON_INFORMATION)

    



class UpdaterFrame(wx.Frame):
    def __init__(self,parent):
        wx.Frame.__init__(self,parent,title="Info Update",size=(300,150), style=wx.MINIMIZE_BOX|wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.padre = parent
        panel = wx.Panel(self)
 
        h_sizer = wx.BoxSizer(wx.HORIZONTAL)
        main_sizer = wx.BoxSizer(wx.VERTICAL)
     
        a,b = check()
        if a:
            texto = "The software is updated"
            btt = "Close"
            moretext = ""
            fun = self.Cerrar
        elif b:
            texto = "Found updated"
            btt = "Update"
            moretext = 30*" "
            fun = self.Update
        else:
            texto = "New version is not automatically updateable"
            btt = "Download"
            moretext = ""
            fun = self.Descarga
           
            
        tex = wx.StaticText(panel,-1,texto,style=wx.CENTER)
        btn = wx.Button(panel, label=btt)
        self.info = wx.StaticText(panel,-1,moretext,style=wx.CENTER)
        
       
        main_sizer.Add((0,0), 1, wx.EXPAND)
        main_sizer.Add(tex,1,wx.CENTER)   
        main_sizer.Add((0,0), 1, wx.EXPAND)
        h_sizer.Add(btn, 0, wx.CENTER)
        main_sizer.Add(h_sizer, 1, wx.CENTER)
        main_sizer.Add((0,0), 1, wx.EXPAND)
        main_sizer.Add(self.info, 1, wx.CENTER,10)
        main_sizer.Add((0,0), 1, wx.EXPAND)
 
        panel.SetSizer(main_sizer)
 
        self.Bind(wx.EVT_BUTTON, fun, btn)
        self.Show()
        
    def Cerrar(self,evt):
        """
        Close update window
        """
        self.Close(True)

    def Update(self,evt):
        
        tmpdir,zipdir = download()
        self.info.SetLabel('Installing ...')      
#        zipdir = 'toptimiz3D-' + RAMA
        instalador(os.path.join(tmpdir,zipdir),self.padre.mainpath,self)
        
    
    def Descarga(self,evt):
        """
        Download and uncompress new version
        """
        
        dlg = wx.DirDialog (None, "Choose directory", "",
                    wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            dirname = dlg.GetPath()
            filename = "toptimiz3D-" + RAMA + '.zip'
            url = "https://gitlab.com/e-aranda/toptimiz3D/-/archive/master/"  + RAMA + '/' + filename
            try:
                r = requests.get(url)
            except IOError as e:
                print("Can't retrieve file")
                return    
            
            filezip = os.path.join(dirname,filename)
            open(filezip,'wb').write(r.content)
            try:
                z = zipfile.ZipFile(filezip)
            except zipfile.error as e:
                print("Bad zipfile (from {0}): {1}".format(url, e))
                return
         
            z.extractall(dirname)
            dirname = z.namelist()[0]
            z.close()   
            self.Cerrar(evt)
