#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
from itertools import islice

class Conversor(object):
    """
    Class to convert COMSOL text file mesh in
    FreeFem++ format 
    """
    def __init__(self,fichero,salida="salida.msh"):
        """
        Given a file name, produces the conversion
        """
        self.archivo = fichero
        self.salida = salida
        
        # do the extraction and modification of data
        mesh = self.extraccion()        
        
        # write the data
        self.escribir(mesh)
        
    def extraccion(self):
        """
        Built a dictionary with all mesh information
        """
        malla = {}
        malla['puntos'] = []
        
        with open(self.archivo) as myFile:
            # Read dimension
            for line in myFile:
                if "# sdim" in line:
                    self.dim = int(line.split()[0])
                    break
            print("Dimension detected: {0:1d}".format(self.dim))
            
            # Read mesh points
            myFile.seek(0)
            for num,line in enumerate(myFile):
                if "number of mesh points" in line:
                    n = int(line.split()[0])
                if "Mesh point coordinates" in line:
                    nummesh = num
                    break                    
                    
            myFile.seek(0)
            for line in islice(myFile,nummesh+1,nummesh+n+1):
                malla['puntos'].append(line[:-2])
             
            # Read triangles   
            cadena = "3 # number of nodes per element"            
            malla['tri'],malla['index_tri'] = self.reading_object(myFile,cadena)            
            
            # Read tetrahedra of edges
            if self.dim == 3:
                # Read tetrahedra
                cadena = "4 # number of nodes per element"            
                malla['tet'],malla['index_tet'] = self.reading_object(myFile,cadena)
            elif self.dim == 2:                                
                # Read edges                
                cadena = "2 # number of nodes per element"            
                malla['edg'],malla['index_lin'] = self.reading_object(myFile,cadena)                    

            malla['nv'] = len(malla['puntos'])
            malla['nt'] = len(malla['tri'])
            if self.dim == 2:
                malla['ne'] = len(malla['edg'])
            elif self.dim == 3:
                malla['nte'] = len(malla['tet'])
                
        return malla
    

    def reading_object(self,myFile,cadena):
        """
        Read object from myFile and modifies it
        """
        lista_objeto = []
        lista_index = []
        # read line where object start
        myFile.seek(0)
        for num,line in enumerate(myFile):
            if cadena in line:
                tri = num 
                break            
        # read number of objects
        myFile.seek(0)        
        for line in islice(myFile,tri+1,tri+2):
            numtri = int(line.split()[0])            
        # read objects
        myFile.seek(0)
        for line in islice(myFile,tri+3,tri+numtri+3):
            lista_objeto.append(line[:-2])
        # read geometry information of object
        myFile.seek(0)
        for line in islice(myFile,tri+numtri+6,tri+2*numtri+6):
            lista_index.append(line[:-2])   
            
        # change numbering
        lista_mod = self.change_number(lista_objeto)
        return lista_mod,lista_index

            
    def change_number(self,old):
        """
        Change numbering (summing by 1)
        """
        new = []
        for x in old:
            new.append(' '.join(map(str,map(lambda y:y+1,map(int,x.split())))))
        return new
        
           
    
    def escribir(self,mesh):
        """
        Write to a file
        """
        with open(self.salida,'w') as f:
        
        # First line: number of elements of each type
            if self.dim == 2:
                f.write(str(mesh['nv']) + ' ' + str(mesh['nt']) + ' ' + str(mesh['ne']) + '\n')
            elif self.dim == 3:
                f.write(str(mesh['nv']) + ' ' + str(mesh['nte']) + ' ' + str(mesh['nt']) + '\n')
 
        # Write points mesh
            for i in mesh['puntos']:
                f.write(i + ' 0\n')
            # Write triangles and edges or tetrahedra and triangles
            if self.dim == 2:
                for i,j in zip(mesh['tri'],mesh['index_tri']):
                    f.write(i + ' ' + j + '\n')
                for i,j in zip(mesh['edg'],mesh['index_lin']):
                    f.write(i + ' ' + j + '\n')
            elif self.dim == 3:
                for i,j in zip(mesh['tet'],mesh['index_tet']):
                    f.write(i + ' ' + j + '\n')
                for i,j in zip(mesh['tri'],mesh['index_tri']):
                    f.write(i + ' ' + j + '\n')
                
if __name__ == "__main__":
    
    entrada = sys.argv[1]
    salida = entrada.replace('.mphtxt','.msh')
    w = Conversor(entrada,salida)             
