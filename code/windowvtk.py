#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  1 15:42:03 2018

@author: earanda
"""
import shutil
import os
import vtk
import wx
from vtk.wx.wxVTKRenderWindowInteractor import wxVTKRenderWindowInteractor
  

class MyWindowVTK(wx.Panel):
    def __init__(self,parent,file_name,tipo):
    
        wx.Panel.__init__(self, parent)
        # file to render and tipo (mesh or density)
        self.filename = file_name
        self.tipo = tipo%2
        # the window 
        self.widget = wxVTKRenderWindowInteractor(self, -1)
        self.widget.Enable(1)
       
        # sizer
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.widget, 1, wx.EXPAND)
        self.SetSizer(self.sizer)
        self.Layout()
            
    def renderthis(self):
        """    
        open a window and create a renderer
        """
        ren = vtk.vtkRenderer()
        ren.SetBackground(0.98, 0.98, 0.98)

        self.widget.GetRenderWindow().AddRenderer(ren)
        
        # read data from file
        reader = vtk.vtkUnstructuredGridReader()
        reader.SetFileName(self.filename)
        reader.Update()
        output = reader.GetOutput()
        
        # mapper        
        mapper = vtk.vtkDataSetMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(output)
        else:
            mapper.SetInputData(output)
        
        # actor
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)  
        
        # choose between showing the mesh or the density
        if not self.tipo:
            mapper.ScalarVisibilityOff()
            actor.GetProperty().SetRepresentationToWireframe()
        else:
            output.GetCellData().SetScalars(output.GetCellData().GetArray(1))            
            lut = vtk.vtkLookupTable()
            lut.SetTableRange(0,100)
            lut.SetHueRange(0, 0)
            
            lut.SetSaturationRange(0, 0)
            lut.SetValueRange(1., 0.)
 
            mapper.SetLookupTable(lut)
            actor.GetProperty().SetRepresentationToSurface()
        
        actor.GetProperty().SetColor((0.,0.,0.))
        ren.AddActor(actor)

        ren.ResetCamera()
        ren.ResetCameraClippingRange()
        cam = ren.GetActiveCamera()
        cam.Elevation(0)
        cam.Azimuth(0)           
 
class NewFrame(wx.Frame):
    def __init__(self,parent,title,filename,tipo,padre):
        wx.Frame.__init__(self,parent,title=title,size=(650,600), style=wx.MINIMIZE_BOX|wx.SYSTEM_MENU|
                  wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.file =filename
        self.tipo = tipo
        self.padre = padre
        p1 = MyWindowVTK(self,self.file,self.tipo)         
        p1.renderthis()
        
                # File menu
        menubar = wx.MenuBar()
        
        fileMenu = wx.Menu()
        m_load = fileMenu.Append(-1,"Save VTK")
        if self.tipo%2:
            m_save = fileMenu.Append(-1,"Save Numeric Result")
            self.Bind(wx.EVT_MENU, self.saveNum, m_save)
        elif self.tipo:
            m_mesh = fileMenu.Append(-1,"Save Mesh")
            self.Bind(wx.EVT_MENU, self.saveMesh,m_mesh)
        m_salir = fileMenu.Append(-1,"Close")
        menubar.Append(fileMenu,"File")
 
        self.SetMenuBar(menubar)        
         
        self.Bind(wx.EVT_MENU, self.salir, m_salir)
        self.Bind(wx.EVT_MENU, self.saveVTK, m_load)   
        
        
    def salir(self, evt):
        self.Close(True)
        
    def saveNum(self, evt):
        print(self.file)
        
    
    def saveVTK(self, evt):
        """
        Save VTK result file
        """
        dlg = wx.FileDialog(self, "Save VTK File", self.padre.mainpath, "", 
                                       "VTK files (*.vtk)|*.vtk", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            shutil.copyfile(self.file,filename)
        else:
            return
        dlg.Destroy()

    def saveMesh(self, evt):
        """
        Save adapted mesh file
        """

        name = 'adaptmesh' + str(self.tipo//2) + '.msh'
        file = os.path.join(self.padre.values['path'],name)
        
        dlg = wx.FileDialog(self.padre, "Save Mesh File", self.padre.mainpath, "", 
                                       "MSH files (*.msh)|*.msh", 
                                       wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            filename = dlg.GetPath()
            shutil.copyfile(file,filename)
        else:
            return


class MyWindow1DVTK(wx.Panel):
    def __init__(self,parent,file_name):
    
        wx.Panel.__init__(self, parent)
        # file to render and tipo (mesh or density)
        self.filename = file_name

        # the window 
        self.widget = wxVTKRenderWindowInteractor(self, -1)
        self.widget.Enable(1)
       
        # sizer
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.widget, 1, wx.EXPAND)
        self.SetSizer(self.sizer)
        self.Layout()
            
    def renderthis(self):
        """    
        open a window and create a renderer
        """

        ren = vtk.vtkRenderer()
        ren.SetBackground(0.98, 0.98, 0.98)

        self.widget.GetRenderWindow().AddRenderer(ren)
        
        view = vtk.vtkContextActor()
#        view.GetRenderer().SetBackground(1.0, 1.0, 1.0)
#        view.GetRenderWindow().SetSize(600, 600)
        
        chart = vtk.vtkChartXY()
        view.GetScene().AddItem(chart)
        
        chart.GetTitleProperties().SetFontSize(16)
        chart.GetAxis(vtk.vtkAxis.BOTTOM).SetTitle('')
        chart.GetAxis(vtk.vtkAxis.LEFT).SetTitle('')
        chart.SetShowLegend(True)
        
        table = vtk.vtkTableReader()
        table.SetFileName(self.filename)
        table.Update()
        output = table.GetOutput()
        
        threshold = output.GetColumn(3).GetValue(0)
        chart.SetTitle("Threshold: "+ '{:1.4f}'.format(threshold))
        
        line1 = chart.AddPlot(vtk.vtkChart.LINE)
        line1.SetInputData(output, 0, 1)
        line1.SetColor(0, 0, 255, 255)
        line1.SetWidth(3.0)
        
        line2 = chart.AddPlot(vtk.vtkChart.LINE)
        line2.SetInputData(output, 0, 2)
        line2.SetColor(255, 0, 0, 255)
        line2.SetWidth(1.0)
        line2.GetPen().SetLineType(vtk.vtkPen.DASH_DOT_LINE)
        ren.AddActor(view)
#        view.GetRenderWindow().SetMultiSamples(0)

        

class New1DFrame(wx.Frame):
    def __init__(self,parent,filename,title):
        wx.Frame.__init__(self,parent,title=title,size=(650,600), style=wx.MINIMIZE_BOX|wx.SYSTEM_MENU|
                  wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN)
        self.file =filename
        p1 = MyWindow1DVTK(self,filename)         
        p1.renderthis()
        

