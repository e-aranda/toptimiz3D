# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:50:38 2017

@author: earanda
"""
import wx
#from extfun import string2number
import paneltitle

# ---------------------------------------------------------------------------   
class MyPanelEntry(wx.Panel):
    """
    Panel containing a vertical panel entries for input data (parameters)
    The panel cotains one panel for each entry (paneles[i]), one sizer in each
    panel (sizers[i]), an static text (static[i] and a TextCtrl (static_e[i])
    """
    def __init__(self,parent,padre,names,var,title):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        
#        self.conversor = string2number
        self.padre = padre
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        self.paneles = []
        sizers = []
        static = []
        self.names = var
        self.static_e = []
        # creating panels
        for i,x in enumerate(names):
            self.paneles.append(wx.Panel(self,style=wx.BORDER_SUNKEN))
            sizers.append(wx.BoxSizer(wx.HORIZONTAL))
            static.append(wx.StaticText(self.paneles[i],-1,x))
            self.static_e.append(wx.TextCtrl(self.paneles[i],-1,str(self.padre.values[var[i]]),\
                                             style=wx.TE_PROCESS_ENTER|wx.TE_RICH))
            sizers[i].Add(static[i],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            sizers[i].Add(self.static_e[i],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            m_sizer.Add(self.paneles[i], 0, wx.TOP|wx.EXPAND , 1)
            self.paneles[i].SetSizer(sizers[i])
            # bindins
            self.Bind(wx.EVT_TEXT, lambda event, ind=i: self.set_val(event,ind), self.static_e[i])
            
        self.SetSizer(m_sizer)
        

        
    def set_val(self,event,i):
        """
        Event for edition of data entries. Set values in MainFrame
        Connected to MainFrame
        """
        self.padre.values[self.names[i]] = event.EventObject.GetValue()
        
        # link volume fraction with initial constant
        if self.names[i] == 'volfrac':
            self.padre.values['init_cte'] = self.padre.values['volfrac']
            self.padre.initpanel.cte.SetValue(self.padre.values['init_cte'])

    
        
