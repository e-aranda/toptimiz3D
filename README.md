# toptimiz3D
Graphical User Interface for designing topological optimization problems to solve with FreeFem++

Available for Linux and Mac

### Two branches:

  - stable-py2: (for Python 2)
    - requires modules: `wxpython` (3.*), `vtk` (> 5.0), `pexpect`, `numpy`, 
                        `requests`, `dictdiffer`

  - stable-py3: (for Python 3)
    - requires modules: `wxpython` (4.*), `vtk` (> 5.0), `pexpect`, `numpy`,
                        `requests`, `dictdiffer`

### Also needs:

  - `FreeFem++`
  - `ff-c++` (FreeFem++ compiler)

### INSTALL:

For installation, clone the stable branch for Python 2 or 3 and write in a terminal:

```sh
python install.py
```

