#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Install script for toptimiz3D
"""
import importlib
import argparse
import sys
import os
import shutil
import tempfile
import platform
import subprocess
from subprocess import getoutput

def printred(s):
    print('\033[91m' + s + '\033[0m')

def printgreen(s):
    print('\033[92m' + s + '\033[0m')

# arguments
install = True
parser = argparse.ArgumentParser()
parser.add_argument('dir', nargs='?', default=None, 
                    help = "Optional installation directory")
parser.add_argument('--nopref',action="store_false",default=True,
                    help='Do not overwrite freefem preferences file')
args = parser.parse_args()


############################ 
# check platform
if platform.system().lower() != "linux" and platform.system().lower() != "darwin":
    print("Platform not supported")
    install = False
else:
    print("Platform: " + platform.system()+'\n')
############################
    
    
############################    
# Check modules installed
try:
    import vtk
    printgreen("vtk is installed in version " + vtk.vtkVersion.GetVTKVersion())
    if int(vtk.vtkVersion.GetVTKVersion().split('.')[0]) < 6:
        printred("vtk version is not correct. Minimum version required is 6")
        install = False
except ImportError:
    printred("VTK module is required")
    install = False

try:
    import wx
    printgreen("WxPython is installed in version " + wx.version().split()[0])
    if int(wx.version()[0]) != 4:
        printred("WxPython version is not correct. Version required: 4")
        install = False
except ImportError:
    printred("WxPython module is required")
    install = False

for x in ["numpy","pexpect","requests","dictdiffer"]:
    try:
        importlib.import_module(x)
        printgreen(x+" is installed in version " + importlib.import_module(x).__version__)
    except ImportError:
        print(x+ "module is required")
        install = False

#############################
# Check is FreeFem++ exists
try:
    res = subprocess.check_output(['which','FreeFem++'])
    ver = 'FreeFem++ | grep version | cut -d "-" -f 2 | cut -d "(" -f 1 | cut -d " " -f 3'
    version = getoutput(ver)
    major,minor = version.split('.')
    printgreen("FreeFem++ is installed in version {0}.{1}".format(major,minor))
    if int(major) < 4:
        printred('FreeFem++ version seems to old. Please update.')
        install = False
except:
    printred("FreeFem++ is not installed")
    install = False


# Check if modules for FreeFem are installed (with temp files)
comando = 'FreeFem++ -ne {temporal} > ' + os.path.devnull
err = []
modulos = ['iovtk','MUMPS_seq','ff-NLopt','ff-Ipopt','medit','gsl']
fich = """
{load} "{mod}";
cout << "OK" << endl;
"""
for t in modulos:
    with tempfile.NamedTemporaryFile() as temp:
        temp.write(fich.format(load='load',mod=t).encode())
        temp.flush()
        err.append(os.system(comando.format(temporal=temp.name)))
with tempfile.NamedTemporaryFile() as temp:
    temp.write(fich.format(load='include',mod='getARGV.idp').encode())
    temp.flush()
    err.append(os.system(comando.format(temporal=temp.name)))
modulos.append('getARGV')
if sum(err) == 0:
    printgreen("FreeFem++ has the necessary modules")
else:
    for i,x in enumerate(err):
        if x != 0:
            printred("Module {0} is missing".format(modulos[i]))
    install = False
#os.remove('test.edp')
##############################

# Check is FreeFem compiler is installed
try:
    res = subprocess.check_output(['which','ff-c++'])
    printgreen("FreeFem++ compiler is installed")
    is_compiler = True
except:
    printred("FreeFem++ compiler is not installed. Conic filter will not work")
    is_compiler = False


if not install:
    printred("\nToptimiz3D cannot be installed")
    printred("Check problems above")
    sys.exit(0)
########################################

#######################################
# base folders
if args.dir:
    folder = args.dir
    check_folder = False
    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
            check_folder = False
        except OSError as e:
            print("Invalid folder:", e.args)
            check_folder = True
else:
    folder =  os.path.join(os.path.expanduser('~'),'toptimiz3D')
    check_folder = True
basefolder = folder
print('')
while check_folder:
    carpeta = input('Destination folder? ['+folder+'] ')
    if carpeta != '':
        folder = carpeta

    if not os.path.exists(folder):
        try:
            os.makedirs(folder)
            check_folder = False
        except OSError as e:
            print("Invalid folder:", e.args)
            print("Try again")
            folder = basefolder
    else:
        check_folder = False

# Create folder for .py files
codefolder = os.path.join(folder,'code')
if not os.path.exists(codefolder):
    try:
        os.makedirs(codefolder)
    except OSError as e:
        raise Exception(e.args)

# Copy VERSION and LICENSE files
for x in ('LICENSE','VERSION'):        
    dst = os.path.join(folder,x)
    print("Copying " + x)
    try:
        shutil.copyfile(x,dst)
    except IOError as e:
        raise Exception(e.args)

# Copying Python files and icons
for x in os.listdir('code'):
    src = os.path.join('code',x)
    dst = os.path.join(codefolder,x)
    if not os.path.isdir(src):
        print("Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)
    else:
        try:
            os.makedirs(dst)
            print("Directory " + x + " created")
        except OSError as e:
            print("Enter in " + x + " ...")
        for y in os.listdir(src):
            print("  Copying " + y + " ...")
            ini = os.path.join(src,y)
            dst = os.path.join(folder,ini)
            try:
                shutil.copyfile(ini,dst)
            except IOError as e:
                raise Exception(e.args)



# Example folder
print("Copying Examples ...")        
examplefolder = os.path.join(folder,'examples')
if not os.path.exists(examplefolder):
    try:
        os.makedirs(examplefolder)
        print("Directory examples created")
    except OSError as e:
        raise Exception(e.args)

# Copying examples
for x in os.listdir('examples'):
    src = os.path.join('examples',x)
    dst = os.path.join(examplefolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)





# Create folder for FreeFem++ files
freefemfolder = os.path.join(folder,'freefems')
if not os.path.exists(freefemfolder):
    try:
        os.makedirs(freefemfolder)
    except OSError as e:
        raise Exception(e.args)
#############################################
        
############################################        
# Compilation of conic filter
if is_compiler:
    print("\nCompiling conic filter")
    compiler_command = 'cd freefems; ff-c++ ConvolutionOrig.cpp; cd ..'
    if os.system(compiler_command):
        printred("Conic filter cannot be compiled")
    else:
        if platform.system().lower() == "linux":
            src = os.path.join('freefems','ConvolutionOrig.so')
            dst2 = os.path.join(freefemfolder,'ConvolutionOrig.so')
        elif platform.system().lower() == "darwin":
            src = os.path.join('freefems','ConvolutionOrig.dylib')
            dst2 = os.path.join(freefemfolder,'ConvolutionOrig.dylib')

# Copying FreeFem files
print("\nCopying FreeFem++ files:")
dst1 = os.path.join(freefemfolder,'colormap.idp')
try:
    print("  Copying colormap.idp ...")
    shutil.copyfile(os.path.join('freefems','colormap.idp'),dst1)
    if is_compiler:
        print("  Copying " + src.split(os.path.sep)[-1] + " ...")
        shutil.copyfile(src,dst2)
        # delete objects files
        os.remove(src)
        os.remove(os.path.join('freefems','ConvolutionOrig.o'))
except IOError as e:
    raise Exception(e.args)
################################################
    
        
    
################################################    
# Setting environment variables for FreeFem
if args.nopref:
    print("\nSetting environment variables in .freefem++.pref file")
    freefemfile = """
    loadpath += "{freefemfolder}"
    includepath += "{freefemfolder}"
    """
    destfile = os.path.join(os.path.expanduser('~'),'.freefem++.pref')
    com = "echo '" + freefemfile.format(freefemfolder=freefemfolder) + "' > " + \
          destfile

    if not os.path.isfile(destfile):      
        os.system(com)
    else:
        asking = True
        while asking:
            answer = input('Do you want to append (a) paths to the file or \
overwrite (default) .freefem++.pref file? [a/O] ')
            if answer in 'aA':
                com = "echo '" + freefemfile.format(freefemfolder=freefemfolder) \
                      + "' >> " + destfile
                os.system(com)
                asking = False
            elif answer == '' or answer in 'oO':
                os.system(com)
                asking = False
            else:
                print("Please answer 'a' or 'o'")
################################################

                

        


#################################################

# Creating desktop launcher
asking = True
while asking:
    answer = input('Do you want to create a Desktop Launcher? [Y/n] ')
    if answer == '' or answer in 'yY':

        if platform.system().lower() == "linux":
            print("Creating Desktop Launcher for Linux")
            # Determine the name of Desktop folder
            try:
                desktopfolder = subprocess.check_output(['xdg-user-dir', 'DESKTOP'])[:-1]
            except:
                desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')

            lanzador = \
"""
[Desktop Entry]
Name=Toptimiz3d
Exec=bash -i {pathtolauncher}
Terminal=false
StartupNotify=true
Type=Application
Icon={icon}
"""

            launcher = \
"""
#!/usr/bin/bash
cd {home}
{path}
"""
            
            # create executable desktop file
            appfile = os.path.join(desktopfolder.decode('utf-8'),'Toptimiz3D.desktop')
            appcommand = getoutput('which python3') + ' ' + os.path.join(folder,'code','toptimiz3d.py')
            applauncher = os.path.join(folder,'code','icons','launcher.sh')
            with open(applauncher,'w') as f:
                f.write(launcher.format(home=os.path.expanduser('~'),path=appcommand))
            dst = os.path.join(folder,'code','icons','topicon.gif')
            
            with open(appfile,'w') as f:
                f.write(lanzador.format(pathtolauncher=applauncher,icon=dst))
            os.chmod(appfile,0o755)
            os.chmod(applauncher,0o755)

        elif platform.system().lower() == "darwin":
            print("Creating Desktop Launcher for MacOS")
            desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')
            shellscript = 'do shell script "{path} ; launchctl setenv PATH $PATH; cd ' + os.path.join(folder,'code') + ' ;  {command}"'

            iconscript = """
#!/bin/sh
# Take an image and make the image its own icon:
sips -i {png}
# Extract the icon to its own resource file:
DeRez -only icns {png} > tmpicns.rsrc
# append this resource to the file you want to icon-ize.
Rez -append tmpicns.rsrc -o {filespecial}
# Use the resource to set the icon.
SetFile -a C {file}
SetFile -a V {filespecial}
# clean up.
rm tmpicns.rsrc
"""
            appfile = os.path.join(desktopfolder,'Toptimiz3D.app')
            special = "$'" + appfile + r"/Icon\r'"
            setpath = 'PATH='+ getoutput('which python')[:-7] +':' \
            + getoutput('which FreeFem++')[:-10] +':$PATH '
            appcommand = 'pythonw ' + os.path.join(folder,'code','toptimiz3d.py')
            with open('shellscript.scpt','w') as f:
                f.write(shellscript.format(path=setpath,command=appcommand))
            os.system('osacompile -o ' + appfile + ' shellscript.scpt')
            # set icon to code folder
            icon = os.path.join('code','icons','topicon.gif')
            with open('iconscript.sh','w') as f:
                f.write(iconscript.format(png=icon,file=appfile,filespecial=special))
            os.system('sh iconscript.sh')
            os.system('rm -f shellscript.scpt iconscript.sh')

        asking = False
    elif answer in 'nN' or answer == '':
        asking = False
    else:
        print("Please, answer y or n")



print("\nInstallation is done")

